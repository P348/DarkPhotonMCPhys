This directory contains classes and routines that have to be removed in future
releases. Some of these materials can be obsolete or too obtrusive.

Content:
    goo_xform.tcc -- the Goo's template X-Form classes implementing
    normalization container.
