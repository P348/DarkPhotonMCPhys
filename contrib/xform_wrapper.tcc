/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_DPHMC_XFORM_WRAPPER_H
# define H_DPHMC_XFORM_WRAPPER_H

# include <cstdint>
# include <cmath>
# include <ostream>
# include <cmath>
# include "goo_xform.tcc"
# include "dphmc-logging.h"

/**@file p348_xform.tcc
 * @brief A yet another implementation of rectangular region automatically
 * adjusting its limits to cover provided points.
 * */

namespace DPhMC {
namespace aux {

// TODO: rename it to avoid misunderstanding as this IS NOT an
// X-Form actually.
template<uint8_t TD,
         typename FloatT=double>
class XForm : public ::goo::aux::NormedMultivariateRange<goo::aux::Range<FloatT>, TD> {
public:
    //using ::goo::aux::NormedMultivariateRange<FloatT, TD>::AbstractRange::LimitIndex;
    enum RangeIdx { min = 0, max = 1 };
public:
    XForm() {
        for( uint8_t i = 0; i < TD; ++i ) {
            this->set_range_instance( i, *(new goo::aux::Range<FloatT>()) );
        }
    }
    ~XForm() {
        for( uint8_t i = 0; i < TD; ++i ) {
            delete &(this->range(i));
        }
    }
    // This methods are added to provide backward compatibility:
    void recalculate_scales() const { /* dop nothing */ }
    virtual void renorm( FloatT * x ) const { this->denorm( x ); }
    virtual void update_ranges( const FloatT * x ) { this->extend_to( x ); }
    virtual FloatT get_limit( RangeIdx ridx, uint8_t d ) const {
        auto & rangeRef = this->range(d);
        if( min == ridx ) {
            return rangeRef.lower();
        } else {
            return rangeRef.upper();
        }
    }
    virtual void set_ranges( const FloatT * xMins, const FloatT * xMaxs ) {
        this->set_limits( xMins, xMaxs );
    }
};

template<uint8_t TD, typename FloatT>
std::ostream & operator<<( std::ostream & /*os*/, const XForm<TD, FloatT> & /*xf*/ ) {
    _TODO_
}

}  // namespace aux
}  // namespace DPhMC

# endif  // H_DPHMC_XFORM_WRAPPER_H


