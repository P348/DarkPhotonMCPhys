/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_DPHMC_EVENT_GENERATOR_CROSS_SECTION_STORAGING_CACHE_H
# define H_DPHMC_EVENT_GENERATOR_CROSS_SECTION_STORAGING_CACHE_H

/**@brief A boost multimap example.
 * @file aprimeCSCache.hpp
 *
 * Using Boost nultimap facility for indexing cross-sections
 * rather than implementing own hashing database seemed like a
 * good idea.
 *
 * This file slightly changes behaviour if STANDALONE_BUILD
 * is set. Those changes allows to test the caching class inside
 * a single utility implemented right in aprimeCSCache.cpp.
 */

# include "dphmc-config.h"

# ifdef DPhMC_TABULATED_GENERATORS

# include "dphmc-exception.hpp"

# if !defined(NDEBUG)
#   define BOOST_MULTI_INDEX_ENABLE_INVARIANT_CHECKING
#   define BOOST_MULTI_INDEX_ENABLE_SAFE_MODE
# endif

# include <boost/multi_index_container.hpp>
# include <boost/multi_index/ordered_index.hpp>
# include <boost/multi_index/ranked_index.hpp>
# include <boost/multi_index/member.hpp>
# include <boost/multi_index/random_access_index.hpp>
# include <boost/multi_index/composite_key.hpp>
# include <set>

namespace bmi = boost::multi_index;

# ifdef STANDALONE_BUILD
#   include <cstdlib>
#   include <iostream>
class TRandom;  // FWD
# else
#   include "evGen/aprimeEvGen.hpp"
# endif

namespace DPhMC {

# ifdef STANDALONE_BUILD
// A dummy class active only in standalone build. An ersatz
// for real APrimeGenerator.
struct APrimeGenerator {
    double q, m, E;
    uint8_t Z;
    APrimeGenerator( double q_, double m_, double E_, uint8_t Z_ ) :
            q(q_), m(m_), E(E_), Z(Z_) { }
    double integral_sum() const { return 0.; }
    void initialize(TRandom *) {}
};  // dummy
# endif  // STANDALONE_BUILD


/**@brief Singleton caches storage for A' cross-sections.
 * @class APrimeCSCaches
 *
 * This class implements a dynamic caching storage of pre-computed
 * cross-section values indexed by a subset of parameters. This class
 * utilizes routines from evGen/ directory.
 *
 * The following assumptions are taken into account:
 *  - projectile energy is a continious real value while projectile charge,
 *    mass and nucleus Z will be restricted by particle and material sorts
 *    sets available during application run.
 *  - is must be available to user code to adjust the tabulation ranges of
 *    continious value for fine tuning.
 *
 * This class can sometimes require a significant amount of memory, if there
 * are a lot of different charged projectiles and materials are available in
 * model. However, most of the charged particles in model are rare and can
 * never be involved in run, so by applying lazy cache initialization one
 * can avoid most of the pretty useless data.
 */
class APrimeCSCaches {
public:
    /// Mass of A' that will be used for all generators created by this
    /// cache class. Must be set to positive value before first invokation
    /// of self() method.
    static double APrimeMass;
    /// Mixing constant of A' photoproduction process that will be used
    /// in all generators created by this cache class. Must be set to positive
    /// value before first invokation of self() method.
    static double MixingConstant;
    /// The multiplication factor has to be set to 1 in natural applications,
    /// but this leads to extremely rare A' production events. This factor
    /// increases the cross sections for development applications.
    static double CSMultFactor;
public:
    //static struct {
    //    // ...
    //} parameters;

    /**@brief Aux class implementing single generator entry.
     * @class Entry
     *
     * This entry class stores generator pointer (unique) and the subset
     * of corresponding parameters: charge and mass of a projectile particle
     * and charge number of target nuclei.
     * */
    struct Entry {
        APrimeGenerator * generator;  ///< A pointer to generator instance
        double charge,  ///< Projectile charge.
               mass,  ///< Projectile mass.
               incE;  ///< Projectile energy.
        uint16_t A;
        uint8_t Z;  /// Target nuclei charge number.

        /// Ctr. Takes a pointer to created generator instance.
        Entry( APrimeGenerator * g, double q, double m, uint16_t mA, uint8_t mZ, double incidentE ) :
            generator(g), charge(q), mass(m), incE(incidentE), A(mA), Z(mZ) {}
        friend std::ostream & operator<<( std::ostream & os, const Entry & f ) {
            os << "{q=" << f.charge << ", m_proj=" << f.mass
               << ", Z_mat=" << (int) f.Z << (int) f.Z
               << ", E_proj=" << f.incE
               << ", genPtr=" << std::hex << f.generator << "}"
               << std::endl
               ;
            return os;
        }
    };

    /// Cache container type itself.
    typedef boost::multi_index_container<
        Entry,
        bmi::indexed_by<
            bmi::ordered_non_unique<
                bmi::composite_key<
                    Entry,
                    bmi::member<Entry, double,  &Entry::charge>,
                    bmi::member<Entry, double,  &Entry::mass>,
                    bmi::member<Entry, double,  &Entry::incE>,
                    bmi::member<Entry, uint16_t,&Entry::A>,
                    bmi::member<Entry, uint8_t, &Entry::Z>
                >
            >
        >
    > CacheContainer;

private:
    static APrimeCSCaches * _self;
    std::set<double> _projectileERanges;
    CacheContainer _caches;
    TRandom * _rndGenerator;

    APrimeCSCaches();
protected:
    static APrimeGenerator * _new_generator( double q, double m, double E, uint16_t A, uint8_t Z );
    static void _delete_generator( APrimeGenerator * );
    void _clear();
public:
    ///
    static APrimeCSCaches & self();

    /**@brief A getter for suitable generator instance.
     *
     * Search among cached data for most suitable pre-computed
     * generator or create one if there is not.
     *
     * @param incidentCharge projectile charge.
     * @param incidentMass projectile mass.
     * @param incidentE projectile kinetic energy.
     * @param nucleusZ Z-number of an element.
     * */
    APrimeGenerator * generator(
                            double incidentCharge,
                            double incidentMass,
                            double incidentE,
                            uint16_t nucleusA,
                            uint8_t nucleusZ);

    /**@brief Cross-section value getter.
     *
     * Uses a generator() getter to obtain cached generator instance
     * and returns its total CS value.
     *
     * @param incidentCharge projectile charge.
     * @param incidentMass projectile mass.
     * @param incidentE projectile kinetic energy.
     * @param nucleusZ Z-number of an element.
     * */
    double aprime_total_cross_section_for(
                            double incidentCharge,
                            double incidentMass,
                            double incidentE,
                            uint16_t nucleusA,
                            uint8_t nucleusZ ) {
        return generator( incidentCharge, incidentMass, incidentE,
                          nucleusA, nucleusZ )->integral_sum();}

    /// Tabulation setter --- clears all the caches.
    void projectile_energy_tabulation( const std::set<double> & newSet ) {
        _clear();
        _projectileERanges = newSet;
    }

    /// Random generator setter.
    void random_generator( TRandom * );

    /// Random generator getter.
    const TRandom * random_generator() const { return _rndGenerator; }

    /// Tabulation getter.
    const std::set<double> & projectile_energy_tabulation() const {
        return _projectileERanges;
    }

    /// Returns number of generators allocated.
    size_t n_generators() const { return _caches.size(); }

    # ifdef STANDALONE_BUILD
    /// Returns a negative integer, if an error occured.
    static int test_indexing_routines( int seed=130,
                                       size_t nIterations=1e4,
                                       std::ostream & os=std::cout );
    # endif  // STANDALONE_BUILD
};

}  // namespace DPhMC

# endif  // DPhMC_TABULATED_GENERATORS

# endif  // H_DPHMC_EVENT_GENERATOR_CROSS_SECTION_STORAGING_CACHE_H

