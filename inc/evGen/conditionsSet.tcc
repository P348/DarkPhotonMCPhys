/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_DPHMC_CONDITIONS_SET_H
# define H_DPHMC_CONDITIONS_SET_H

# include <unordered_set>

namespace DPhMC {
namespace aux {

/**@brief Silly class designed to imitate some handy functional-style
 *        features in approach of physical validity cut-offs.
 *
 *  MatchResultT should provide implicit deducing from/to bool type (and
 *  unary negotiation, of course).
 *
 *  Note, that ConditionsSet doesn't creates copies of CutMatchCallableT,
 *        so one should take care of their lifetime.
 * */
template<typename ArgsT,
         typename MatchResultT=bool,
         typename CutMatchCallableT=MatchResultT(*)(const ArgsT &)>
class ConditionsSet {
public:
    typedef ArgsT               Arguments;
    typedef CutMatchCallableT   Condition;
    typedef MatchResultT        MatchResult;
protected:
    std::unordered_set<const Condition *> _conditions;
public:
    ConditionsSet() {}
    virtual ~ConditionsSet() {}

    /// Returns true, if x passes all conditions.
    MatchResult every( const Arguments & x ) const {
        for( auto it  = _conditions.begin();
                  it != _conditions.end(); ++it) {
            auto res = (**it)(&x);
            if( ! res ) {
                return res;
            }
        }
        return true;
    }

    /// Returns true, if x passes at least one condition.
    MatchResult one_of( const Arguments & x ) const {
        for( auto it  = _conditions.begin();
                  it != _conditions.end(); ++it) {
            auto res = (**it)(&x);
            if( res ) {
                return res;
            }
        }
        return false;
    }

    /// Adds a condition.
    void add_condition( const Condition & c ) {
        _conditions.insert( &c );
    }

    bool empty() const { return _conditions.empty(); }
};

}  // namespace aux
}  // namespace DPhMC

# endif  // H_DPHMC_CONDITIONS_SET_H

