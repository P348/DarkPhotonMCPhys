/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_APRIME_EVGEN_H
# define H_APRIME_EVGEN_H

# include "dphmc-config.h"

# ifdef DPhMC_TFOAM_GENERATOR

# include <list>

# include <TFoamIntegrand.h>
# include <Math/ParamFunctor.h>
# include <Math/IFunction.h>

# include "evGen/tfoam_distr.tcc"
# include "evGen/conditionsSet.tcc"
# include "evGen/parameters.h"
# include "evGen/aprimeCrossSection.h"
# include "evGen/tfoam_distr.tcc"

# include "xform_wrapper.tcc"

# include "dphmc-runtime-cfg.hpp"

class TH2F;

namespace DPhMC {

/**@class APrimeWWPDF
 * @brief Weizs ̈acker-Williams approach for dark photons according to
 *        \cite{JDBjorken}.
 *
 * Utilizes ROOT TFoamIntegrand class interface as a PDF for event-generation
 * algorithm. Uses numerical routines from aprimeCrossSection.h family
 * combining with our X-Form template providing normalization mapping at
 * working phase space region.
 *
 * This is not yet a generator itself. It is a PDF wrapper for generator.
 */
class APrimeWWPDF : public TFoamIntegrand,
                    public aux::XForm<2, Double_t> {
public:
    typedef TFoamIntegrand Parent;
    typedef aux::XForm<2, Double_t> XForm;
    typedef uint8_t(*CutOffCallback)(const struct APrimeCSCutOffParamsPack *);
    typedef aux::ConditionsSet<
                 const struct APrimeCSCutOffParamsPack,
                 uint8_t,
                 CutOffCallback> Conditions;
protected:
    static Conditions _conditions;
# ifndef APRIME_PDF__NO_STATS_CHECK
public:
    /* The following XForms stores min/max values for normed and
     * unnormed argument values and event weights in order to perform
     * basic validity check. */
    mutable aux::XForm<3, Double_t> _check1,  ///< check interval x, theta, density
                                    _check2,  ///< check interval x_n, theta_n, density
                                    _check3;  ///< check interval x_ng, theta_ng, denisty_g

    /// Easy-to-copy structure referencing APrimeWWPDF instance
    /// for ROOT::Math:: integration.
    class ReferencingProxyDCos : public ROOT::Math::IBaseFunctionMultiDim {
    private:
        const APrimeWWPDF & _instance;
    public:
        ReferencingProxyDCos() = delete;
        ReferencingProxyDCos( const APrimeWWPDF & i ) : _instance( i ) {}
        ReferencingProxyDCos( const ReferencingProxyDCos & o ) : _instance( o._instance ) {}
        ~ReferencingProxyDCos() {}

        /// Required by IBaseFunctionMultiDim.
        virtual IBaseFunctionMultiDim * Clone() const override;

        /// Required by IBaseFunctionMultiDim.
        virtual unsigned int NDim() const override { return XForm::D; }

        /// Required by IBaseFunctionMultiDim.
        virtual double DoEval(const double *) const override;

        virtual double operator()( const double * x ) const {
            return DoEval(x);}
    };

    # if 0
    // XXX _
    class ReferencingProxyByTheta : public ROOT::Math::IBaseFunctionMultiDim {
    private:
        const APrimeWWPDF & _instance;
    public:
        ReferencingProxyByTheta() = delete;
        ReferencingProxyByTheta( const APrimeWWPDF & i ) : _instance( i ) {}
        ReferencingProxyByTheta( const ReferencingProxyByTheta & o ) : _instance( o._instance ) {}
        ~ReferencingProxyByTheta() {}

        /// Required by IBaseFunctionMultiDim.
        virtual IBaseFunctionMultiDim * Clone() const override;

        /// Required by IBaseFunctionMultiDim.
        virtual unsigned int NDim() const override { return XForm::D; }

        /// Required by IBaseFunctionMultiDim.
        virtual double DoEval(const double *) const override;

        virtual double operator()( const double * x ) const {
            return DoEval(x);}
    };
    // XXX^
    # endif

# endif  // APRIME_PDF__NO_STATS_CHECK
private:
    mutable bool _cachesValid;
    // Note: duplicates workspace's parameters
    # define declare_cachevar_phys( type, txtName, name, descr ) \
            mutable type _cache_ ## name; \
            mutable bool _cache_ ## name ## _set;
        for_all_PhMClib_aprimeCS_parameters( declare_cachevar_phys )
    # undef declare_cachevar_phys
    # define declare_cachevar_num( type, txtName, dft, name, descr ) mutable type _cache_ ## name;
        for_all_PhMClib_aprimeCS_GSL_chi_comp_parameter( declare_cachevar_num )
    # undef declare_cachevar_num
    /// An actual workspace.
    mutable void * _cParameters;
    mutable double _integrdCache;
protected:
    void _free_C_workspace();
    double _x_bound( uint8_t ) const;
    double _theta_max() const;
public:
    /// Default ctr; creates an instance that need to be initialized
    /// further in some way.
    APrimeWWPDF();

    /// Copy ctr.
    APrimeWWPDF( const APrimeWWPDF & );

    /// A all-in-one ctr; creates an instance ready to use.
    APrimeWWPDF(
            # define declare_physparameter_arg( type, txtName, name, descr ) type name,
                for_all_PhMClib_aprimeCS_parameters( declare_physparameter_arg )
            # undef declare_physparameter_arg
            # define declare_numsparameter_arg( type, txtName, dft, name, descr ) type name = dft,
                for_all_PhMClib_aprimeCS_GSL_chi_comp_parameter( declare_numsparameter_arg )
            # undef declare_numsparameter_arg
            void * dummy = nullptr
        );

    virtual ~APrimeWWPDF();

    /// Returns true when all input arguments are set.
    bool is_complete() const;

    /// Manual recalculation of cached values.
    void recache() const;

    /// Manual invalidating computed caches.
    void invalidate_cache() const { _cachesValid = false; }

    /// True, if computed caches are valid.
    bool cache_is_valid() const { return _cachesValid; }

    /// Caches recalculation shortcut.
    void recache_if_need() const { if(!cache_is_valid()){ recache(); } }

    /// Registers a cut-off function.
    static void add_cut_off( CutOffCallback );

    /// Returns a Weitzeker-Williams approximation cross-section
    /// (GeV, unnormed args are expected). To obtain picobarn,
    /// use DPhMC::aux::pbarn_per_GeV constant.
    double density( double x, double theta ) const;

    /// Returns a partial integral value in provided ranges. If infinite, XForm ranges
    /// will be used (negative and positive values will be treated as special values for
    /// minimal and maximal ranges correspondingly).
    double probability( double xLow, double xUp, double thetaLow, double thetaUp ) const;

    ///  Returns a partial integral value in entire area of definition.
    double probability() const {
        if( std::isnan( _integrdCache ) ) {
            _integrdCache = probability( - std::numeric_limits<double>::infinity(),
                                           std::numeric_limits<double>::infinity(),
                                         - std::numeric_limits<double>::infinity(),
                                           std::numeric_limits<double>::infinity() );
            assert( ! ::std::isnan(_integrdCache) );
        }
        return _integrdCache;
    }

    /// Returns integral estimation according to (A15) formula \cite{Bjorken}.
    double integral_sum_a15() const;

    # if 0
    /// XXX:
    double integral_XXX() const;
    # endif

    /// Returns x range.
    std::pair<double, double> x_ranges() const;

    //
    // Interface implementation
    //

    /// Implementation of TFoam's Density() virtual method
    /// (returns pbarn, normed args are supposed).
    virtual Double_t Density(int nDim, Double_t * normed_Xarg) override;

    virtual double operator()( const double * x ) const {
        return density(x[0], x[1]);}

    //
    // Aux
    //

    /// Prints out JSON-formatted state of an object.
    void json_dump( std::ostream & os ) const;

    //
    // Data type getters/setters
    //

    # define implement_physparameter_getset( type, txtName, name, descr )           \
            type name() const {                                                     \
            if( ! _cache_ ## name ## _set ) { DPhMC_exception_throw(badState,                     \
                        "Parameter " txtName " is unset while being acquired.");}   \
                return _cache_ ## name; }                                           \
            void name( const type & v ) {                                           \
                DPhMC_msg3( "APrimeWWPDF %p: parameter " txtName " is now %e\n",      \
                                                              this, (double) v );   \
                _cache_ ## name ## _set = true;                                     \
                invalidate_cache();                                                 \
                _cache_ ## name = v; }
        for_all_PhMClib_aprimeCS_parameters( implement_physparameter_getset )
    # undef implement_physparameter_getset
    # define implement_numsparameter_getset( type, txtName, dft, name, descr ) \
            type name() const { return _cache_ ## name; }                           \
            void name( const type & v ) {                                           \
                DPhMC_msg3( "APrimeWWPDF %p: parameter " txtName " is now %e",        \
                                                              this, (double) v );   \
                invalidate_cache();                                                 \
                _cache_ ## name = v; }
        for_all_PhMClib_aprimeCS_GSL_chi_comp_parameter( implement_numsparameter_getset )
    # undef implement_numsparameter_getset

    // XForm compat
    virtual Double_t get_limit( XForm::RangeIdx ridx, uint8_t d ) const override {
        recache_if_need();
        return XForm::get_limit( ridx, d );
    }
    virtual void norm( Double_t * x ) const override {
        recache_if_need();
        XForm::norm(x);
    }
    virtual void renorm( Double_t * x ) const override {
        recache_if_need();
        XForm::renorm(x);
    }
    virtual bool match( Double_t * x ) const override {
        recache_if_need();
        return XForm::match(x);
    }

    static double convert_cross_section_units_to_CLHEP_units( double );
};

namespace aux {
union APrimeGeneratedEvent {
    Double_t x[2];
    struct {
        Double_t x, theta;
    } byName;
};
}  // namespace aux

/**@class APrimeGenerator
 * @brief General-purpose vector-event generator according to \cite{JDBjorken}.
 *
 * Implements the TFoamDistribution class providing event generator facility
 * around PDF described in APrimeWWPDF class.
 */
class APrimeGenerator : public TFoamDistribution<2, aux::APrimeGeneratedEvent> {
public:
    /// Names the TFoam generator interface.
    typedef TFoamDistribution<2, aux::APrimeGeneratedEvent> Parent;
    /// Names the event type to be generated.
    typedef typename Parent::Event Event;
    /// Names the distribution functor class.
    typedef APrimeWWPDF Distribution;
# ifndef APRIME_PDF__NO_EVENTS_STORAGING
public:
    /// Stores generated events.
    mutable std::list<aux::APrimeGeneratedEvent> _generatedEventsHistory;
# endif
private:
    /// PDF functor instance ptr.
    APrimeWWPDF * _integrandInstancePtr;
    bool _externalIntegrandInstance,    ///< if integrand was provided explicitly.
         _isInitialized;                ///< if generator was initialized.
    /// Will be set to true when first event generated.
    mutable bool _eventGenerated;
public:
    APrimeGenerator() = delete;
    /// Comprehensive ctr -- produces internal instance of A'-CS PDF.
    APrimeGenerator(
            const std::string & name,
            # define declare_physparameter_arg( type, txtName, name, descr ) type name,
                for_all_PhMClib_aprimeCS_parameters( declare_physparameter_arg )
            # undef declare_physparameter_arg
            # define declare_numsparameter_arg( type, txtName, dft, name, descr ) type name = dft,
                for_all_PhMClib_aprimeCS_GSL_chi_comp_parameter( declare_numsparameter_arg )
            # undef declare_numsparameter_arg
            # define declare_tfoam_parameter( type, txtName, dft, name, descr ) type name = dft,
                for_all_PhMClib_TFoam_generator_parameters( declare_tfoam_parameter )
            # undef declare_tfoam_parameter
            void * dummy = 0
            );
    /// Imposed PDF ctr.
    APrimeGenerator(
            const std::string & name,
            APrimeWWPDF * pdfPtr,
            # define declare_tfoam_parameter( type, txtName, dft, name, descr ) type name = dft,
                for_all_PhMClib_TFoam_generator_parameters( declare_tfoam_parameter )
            # undef declare_tfoam_parameter
            void * dummy = 0
            );

    /// Leaves PDF functor if it was provided explicitly.
    virtual ~APrimeGenerator();

    /// Integrand getter.
    APrimeWWPDF & integrand();

    /// Integrand getter (const).
    const APrimeWWPDF & integrand() const;

    /// Returns true if at least one event was generated by this instance.
    bool event_generated() const { return _eventGenerated; }

    /// Sets up the validity check before parent call.
    virtual void initialize( TRandom * rndGen ) override;

    /// Does the validity check before parent call.
    virtual double generate( Event & e ) const override;

    /// Returns full integral value acquired according to TFoam, in GeVs.
    double integral_sum() const;

# ifndef APRIME_PDF__NO_EVENTS_STORAGING
    /// Draws a histogram in current context containing
    TH2F * draw_agreement_plot( uint8_t nBinsX, uint8_t nBinsTheta ) const;
# endif
};

}  // namespace DPhMC

# endif  // DPhMC_TFOAM_GENERATOR

# endif  // H_APRIME_EVGEN_H

