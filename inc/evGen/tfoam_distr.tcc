/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_DPHMC_TFOAM_DISTR_H
# define H_DPHMC_TFOAM_DISTR_H

# include "dphmc-config.h"
# include "dphmc-logging.h"

# include <TFoam.h>
# include <TFoamIntegrand.h>

# include "evGen/parameters.h"
# include "dphmc-exception.hpp"

namespace DPhMC {

namespace aux {
/// A parameter set for TFoamDistirbution to shorten signatures.
struct TFoamDistributionParameters {
    size_t  nCells,
            nSamples,
            nBins,
            optRej,
            optEvPerBin;
    uint8_t chatLvl,
            optDrive;           // Maximum weight reduction, =1 for variance reduction;

    TFoamDistributionParameters(
        int8_t chatLvl_ = -1,    // Chat level; -1 -- take from application default.
        size_t nCells_  = 500,   // # of cells (FOAM)
        size_t nSamples_= 200,   // Number of MC events per cell in build-up
        size_t nBins_   = 8,     // Number of bins in build-up
        size_t optRej_  = 1,     // Wted events for OptRej=0; wt=1 for OptRej=1 (default)
        uint8_t optDrive_=2,     // Option, type of Drive =0,1,2 for TrueVol,Sigma,WtMax
        size_t optEvPerBin_= 25  // Maximum events (equiv.) per bin in buid-up
        ) :     nCells(nCells_),
                nSamples(nSamples_),
                nBins(nBins_),
                optRej(optRej_),
                optEvPerBin(optEvPerBin_),
                chatLvl( chatLvl_ < 0 ? G_DPhMC_verbosity : chatLvl_ ),
                optDrive(optDrive_) {}
};
}  // namespace aux

/**@class TFoamDistribution
 * @brief A template wrapper for ROOT::TFoam interface for FOAM generator.
 *
 * Meaning of protected fields and settings are the same as in the original
 * ROOT::TFoam class.
 *
 * Basically, the TFoamDistribution designed to treat the arbitrary
 * ROOT::TFoamIntegrand instance, but in DPhMC library we suppose to use
 * ones which are normalized via our DPhMC::aux::XForm instance/inheritance
 * mixing. Nevertheless, the code of TFoamDistribution doesn't affect any
 * additional interfaces except ROOT::TFoamIntegrand.
 *
 * Note: event type must provide .x[TD] double array.
 */
template<uint8_t TD,
         typename EventTypeT>
class TFoamDistribution : public TFoam {
private:
    TFoamDistribution() = delete;
    TFoamDistribution( const TFoamDistribution & ) = delete;
protected:
    TFoamIntegrand * _integrand;
    # define declare_tfoam_parameter_field( type, txtName, dft, name, descr ) \
        type _ ## name;
    for_all_PhMClib_TFoam_generator_parameters( declare_tfoam_parameter_field )
    # undef declare_tfoam_parameter_field
    TRandom * _rndGeneratorPtr;
public:
    constexpr static uint8_t D = TD;
    typedef TFoam Parent;
    typedef EventTypeT Event;
    typedef TFoamDistribution<TD, EventTypeT> Self;
public:
    /// Meaning of ctr parameters is similar to corresponding in ROOT::TFoam::Initialize.
    TFoamDistribution(
            TFoamIntegrand * integrand,
            const std::string & name,
            # define declare_tfoam_parameter( type, txtName, dft, name, descr ) \
                type name = dft,
            for_all_PhMClib_TFoam_generator_parameters( declare_tfoam_parameter )
            # undef declare_tfoam_parameter
            void * dummy = 0
        ) :
                Parent(name.c_str()),
                _integrand(integrand),
                # define set_tfoam_parameter( type, txtName, dft, name, descr ) \
                _ ## name (name),
                for_all_PhMClib_TFoam_generator_parameters( set_tfoam_parameter )
                # undef set_tfoam_parameter
                _rndGeneratorPtr(nullptr) {  void((void *) dummy);  }

    virtual ~TFoamDistribution( ) { }

    /// Only is needed to provide the initial random generator for initializarion.
    virtual void initialize( TRandom * rndGen ) {
        if( _rndGeneratorPtr ) {
            DPhMC_exception_throw(badState,
                                  "TFoam generator %p repeat initialization.",
                                  (const void*) this);
        }
        if( !_integrand ) {
            DPhMC_exception_throw(badState,
                                  "TFoam generator %p has no any integrand set.",
                                  (const void*) this);
        }
        Parent::SetkDim(        D );
        Parent::SetnCells(      _nCells);
        Parent::SetnSampl(      _nSamples);
        Parent::SetnBin(        _nBins);
        Parent::SetOptRej(      _oRej);
        Parent::SetOptDrive(    _oDrive);
        Parent::SetEvPerBin(    _oEvPerBin);
        Parent::SetChat(        _oChatLvl);

        Parent::SetRho( _integrand );
        Parent::SetPseRan( _rndGeneratorPtr = rndGen );
        Parent::Initialize(); // Go!

        //Parent::CheckAll(1);  // do basic TFoam self-tests printing out all warnings found.
    }

    /// Does actual vector-like event generation.
    virtual double generate( EventTypeT & e ) const {
        if( !_rndGeneratorPtr ) {
            DPhMC_exception_throw(badState, "TFoam generator %p uninitialized.", (const void*) this);
        }
        // Note: unfortunately ROOT developers never care about const-validity...
        //return Parent::MCgenerate( e.x );
        return const_cast<Parent *>(dynamic_cast<const Parent *>(this))->MCgenerate( e.x );
    }

    /// Returns seeding random generator pointer.
    TRandom * rnd_generator_ptr() { return _rndGeneratorPtr; }
    /// Returns seeding random generator pointer (const ver).
    const TRandom * rnd_generator_ptr() const { return _rndGeneratorPtr; }

    template<uint8_t TD_, typename EventTypeT_> friend
    std::ostream & operator<<( std::ostream &, const TFoamDistribution<TD_, EventTypeT_> & );

    // Produces TFoam cellular plot of this instance (like RootPlot2dim() original method).
    // void  draw_cells();
    // todo: use something like RootPlot2dim() to plot the cellular subdiv structure
    // as it useful for debugging.
};

template<uint8_t TD, typename EventTypeT>
std::ostream &
operator<<( std::ostream & os, const TFoamDistribution<TD, EventTypeT> & tfd ) {
    os << "{" << std::endl
       << "     type : " << "\"TFoamDistribution<" << (int) TD << ", ?EventType?>\"," << std::endl
       << "      ptr : " << &tfd << "," << std::endl
       << " distrFun : " << tfd._integrand << ", " << std::endl
       << "   nCells : " << tfd.GetnCells() << "," << std::endl
       << " nSamples : " << tfd._nSamples << "," << std::endl
       << "    nBins : " << tfd._nSamples << "," << std::endl
       << "   optRej : " << tfd._optRej << "," << std::endl
       << " optDrive : " << tfd._optDrive << "," << std::endl
       << " evPerBin : " << tfd._optEvPerBin << ";" << std::endl
       << "}" << std::endl;
    return os;
}

}  // namespace DPhMC

# endif  // H_DPHMC_TFOAM_DISTR_H

