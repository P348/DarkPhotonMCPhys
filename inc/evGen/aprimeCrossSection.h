/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_APRIME_CROSS_SECTIONS_H
# define H_APRIME_CROSS_SECTIONS_H

# include "dphmc-config.h"

/**@file aprimeCrossSection.h
 * @brief Pure C computational routines based on code originally written
 *        by D. Kirpichnikov.
 *
 * Header contains declarations of main computational routines combined
 * in order to provide probability density function (PDF) for production
 * reaction of hypothetical «dark photons» according to \cite{JDBjorken}.
 *
 * Referencies:
 *  - JDBjorken // http://arxiv.org/pdf/0906.0580v1.pdf
 */

# include <stdlib.h>
# include <stdint.h>
# include "evGen/parameters.h"

# ifdef __cplusplus
extern "C" {
# endif

/** Initializes supplementary parameter set for cross-section calculation.
 *
 * Note, that workspace stores all numerical and physical parameters, including
 * beam energy, integration errors, etc., so one need invoke this routine each
 * time them need to be changed.
 *
 * Internally, it does all necessary recalculations.
 *
 * Note: supp parameters pointer destination should be set to NULL for new
 * instance creation.
 *
 * @param n nodes for GSL integration workspace (see GSL QAGS);
 * */
void init_aprime_cross_section_workspace(
    # define declare_arg( type, txtName, name, descr ) type name,
        for_all_PhMClib_aprimeCS_parameters( declare_arg )
    # undef declare_arg
    # define declare_arg( type, txtName, dft, name, descr ) type name,
        for_all_PhMClib_aprimeCS_GSL_chi_comp_parameter( declare_arg )
    # undef declare_arg
    void ** wsPtr );

/** Frees supplementary parameters set for chi integration. */
void free_aprime_cross_section_workspace( void * );

/**@brief Integrated photons flux for A' CS calculations.
 *
 * For given nuclei parameters, calculates integrated photons flux
 * involving GSL integration procedure. On error, raises standard Goo
 * exception with third-party error code.
 *
 * see \ref{JDBjorken} (A17). */
double chi( double * absError,
            double * relError,
            void * ws );

/**@brief Calculates cross-section according to \cite{JDBjorken} (A12).
 *
 * Calculates sigma according to (A12) formula \cite{JDBjorken}, returning
 * result of numerical calculation of the following value:
 * \f[
 * \frac{1}{E_{0}^2 x} \frac{ d \sigma_{ 2 \rightarrow 3 } }{ d x d \cos{\theta_{A'}} }
 * \f]
 *
 * Plase, note, that returning value is not a cross-section itself, but an its
 * value divided by $1/(x*E_{0}^2)$.
 * */
double aprime_cross_section_a12( double x,
                             double theta,
                             void * ws );

/** Returns $x_{min}$ cut (from common sense: $m_{A'}/E_0$) */
double upper_cut_x( void * ws );

/** Returns $x_{max}$ cut (according to formula on p. 4 \cite{Bjorken}) */
double lower_cut_x( void * ws );

/** Returns $\theta_{A', max} cut (according to formula (9) \cite{Bjorken}) $ */
double upper_cut_theta( void * ws );

/** Returns integral estimation obtained according to (A16) formula \cite{Bjorken} */
double analytic_integral_estimation( void * ws );

/**@brief auxiliary struct, used by C++ wrapper as for cut-off conditions*/
struct APrimeCSCutOffParamsPack {
    void * ws;
    double x, theta;
};

# ifdef __cplusplus
}
# endif

# endif  /* H_APRIME_CROSS_SECTIONS_H */

