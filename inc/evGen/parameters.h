/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_DPHMC_PARAMETERS_H
# define H_DPHMC_PARAMETERS_H

# include "dphmc-config.h"

/**@brief File containing X-macro's of all configurable parameters and
 * constants.
 *
 * Each parameter should have data type, name and human-readable description.
 * The default value and textual name are optional.
 * */

# ifndef __cplusplus
#   include <stdlib.h>
#   include <stdint.h>
# else
#   include <cstdlib>
#   include <cstdint>
# endif

/*
 * Constants
 */

/**@macro for_all_PhMClib_numerical_constants
 * @brief All constant X-macro
 *
 * From C++ they're located at DPhMC::aux::* namespace. From C all of them are
 * available as extern constant within _DPhMC_CST_* prefix.
 **/
# define for_all_PhMClib_numerical_constants(m)                             \
    m( double,      electronMass_GeV,           "Electron mass (GeV)" )     \
    m( double,      fineStructureConstant,      "Fine structure constant (1/137)" )  \
    m( double,      muProton,                   "Proton \\mu number" )      \
    m( double,      protonMass_GeV,             "Proton mass (GeV)" )       \
    m( double,      pbarn_per_GeV,              "GeV to pbarn translation factor" ) \
    /* ... */

/*
 * Parameters
 */

/**@macro for_all_PhMClib_aprimeCS_parameters
 * @brief All A' cross-section computation related configurables.
 *
 * This macro implies no default parameters and just claims textual names.
 * */
# define for_all_PhMClib_aprimeCS_parameters( m )                           \
    m( uint16_t,    "materialZ",    Z,          "Target material Z" )       \
    m( double,      "materialA",    A,          "Target material A (mass number) a.e.m," ) \
    m( double,      "massA_GeV",    massA_GeV,  "A' hypothetical mass, GeV" )   \
    m( double,      "EBeam_GeV",    EBeam_GeV,  "Incident (e-)-beam energy, GeV") \
    m( double,      "mixingFactor", epsilon,    "Mixing constant for A' particles." ) \
    m( double,      "factor",       factor,     "(dev) Multiplication factor for cross-section." ) \
    /* ... */

/**@macro for_all_PhMClib_aprimeCS_GSL_chi_comp_parameter
 * @brief All parameters related to integrating of \chi form-factor.
 *
 * This macro has both, the default value and textual name.
 * */
# define for_all_PhMClib_aprimeCS_GSL_chi_comp_parameter( m )               \
    m( double,      "epsabs",       1e-12,      epsabs,     "epsabs Gauss-Kronrod absolute error (see GSL QAGS)" ) \
    m( double,      "epsrel",       1e-12,      epsrel,     "epsrel Gauss-Kronrod relative error (see GSL QAGS)" ) \
    m( double,      "erelst",       1.1,        epsrelIncFt,"epsrel Gauss-Kronrod relative error increasing factor" ) \
    m( size_t,      "limit",        1e3,        limit,      "limit maximum number of subintervals (see GSL QAGS)") \
    m( size_t,      "nnodes",       1e3,        nnodes,     "GSL integration workspace nodes number" )
    /* ... */

/**@macro for_all_PhMClib_TFoam_aprime_distr_parameters
 * @brief Parameters for TFoam A' distribution MC-generator.
 *
 * This macro has both, the default value and textual name.
 * */
# define for_all_PhMClib_TFoam_generator_parameters( m )                    \
    m( size_t,      "nCells",       500,        nCells,     "number of Cells (FOAM)" )  \
    m( size_t,      "nSamples",     200,        nSamples,   "number of MC events per cell in build-up" ) \
    m( size_t,      "nBins",        8,          nBins,      "number of bins in build-up" ) \
    m( bool,        "oRej",         true,       oRej,       "Weighted events for OptRej=0; wt=1 for OptRej=1" ) \
    m( uint16_t,    "oDrive",       2,          oDrive,     "type of Drive =0,1,2 for TrueVol,Sigma,WtMax" ) \
    m( size_t,      "oEvPerBin",    25,         oEvPerBin,  "Maximum events (equiv.) per bin in buid-up" ) \
    m( int16_t,     "chatLevel",    -1,         oChatLvl,   "TFoam verbosity level: 0-3, where 0 is quiet." ) \
    m( uint32_t,    "trand3Seed",   0,          oTRand3Seed,"Seed for TRandom3 rngen (when used by FOAM)." ) \
    /* ... */

/*
 * Shortcuts
 */

/* ... */

/*
 * Implementation
 */

# ifdef __cplusplus
// For C++:
namespace DPhMC {
namespace aux {
# define declare(type, name, descr) extern const type name;
for_all_PhMClib_numerical_constants(declare)
# undef declare
}  // namespace aux
}  // namespace DPhMC
# else
/* For pure C: */
# define declare(type, name, descr) extern const type _DPhMC_CST_ ## name;
for_all_PhMClib_numerical_constants(declare)
# undef declare
# endif

# endif  /* H_DPHMC_PARAMETERS_H */

