/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_DPHMC_RUNTIME_CONFIG_H
# define H_DPHMC_RUNTIME_CONFIG_H

# include "dphmc-config.h"
# include "dphmc-exception.hpp"
# include "dphmc-logging.h"

# include <unordered_map>
# include <boost/any.hpp>
# include <string>
# include <utility>
# include <sstream>

namespace DPhMC {

/**@brief Run-time configuration object keeping parameters for A' physics.
 * @class Config
 *
 * This class implements a singleton instance indexing various run-time 
 * parameters for A' event generation. These parameters steer the generator and
 * are listed in evGen/parameters.h grouped in its functional sections inside
 * of corresponding X-Macros. This class implements common querying interface
 * using boost::any_cast<>().
 * */
class Config :
        public std::unordered_map<std::string,
                                  std::pair<boost::any, std::string>> {
private:
    typedef std::pair<boost::any, std::string> Entry;

    static Config * _self;

    Config();
protected:
    /// Generalized parameters getter.
    virtual boost::any & mutable_option( const std::string & key );
public:
    /// Sets the config option.
    template<typename T>
    void set( const std::string & optName, const T & value) {
        mutable_option( optName ) = value;
        if( G_DPhMC_verbosity > 1 ) {
            std::stringstream ss;
            ss << value;
            DPhMC_msg2( "A' %p config instance: parameter \"%s\" set to %s.\n",
                    this, optName.c_str(), ss.str().c_str() );
        }
    }

    /// Generalized parameters getter.
    virtual boost::any option( const std::string & key ) const;

    /// Typed parameters getter.
    template<typename T> const T option_as( const std::string & key ) const {
        const boost::any ref = option(key);
        try {
            return boost::any_cast<T>(ref);
        } catch( boost::bad_any_cast & ) {
            DPhMC_exception_throw( badCast,
                    "Parameter \"%s\" of run-time config got wrong type cast",
                    key.c_str() );
        }
    }

    /// Returns singleton instance.
    static Config & instance();
};

}  // namespace DPhMC

# endif  // H_DPHMC_RUNTIME_CONFIG_H

