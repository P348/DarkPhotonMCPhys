/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_DPHMC_EXCEPTION_H
# define H_DPHMC_EXCEPTION_H

# include "dphmc-config.h"
# include "dphmc-logging.h"

# include <cstdint>
# include <stdexcept>
# include <string>

namespace DPhMC {

class Exception : public std::runtime_error {
public:
    enum Code : uint8_t {
        # define declare_enum_code( code, name, description ) name = code,
        for_all_DPhMC_error_codes( declare_enum_code )
        # undef declare_enum_code
    };
private:
    Code _code;
public:
    Exception() : std::runtime_error("DPhMC common error."),
                  _code(common)  {}
    Exception( Code code_, const std::string & details ) :
                                std::runtime_error( details ),
                                _code(code_) {}

    Code code() const { return _code; }
};  // class Exception

}  // namespace DPhMC

# define DPhMC_exception_throw( code, fmt, ... ) while(true) {              \
    char bf[256]; snprintf( bf, sizeof(bf), fmt, ## __VA_ARGS__ );          \
    throw DPhMC::Exception( DPhMC::Exception:: code, bf ); break; }

# define _TODO_ DPhMC_exception_throw( unimplemented, \
            "Reached the code part that is not yet implemented." );

# endif  // H_DPHMC_EXCEPTION_H

