/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_DPHMC_LOGGING_H
# define H_DPHMC_LOGGING_H

# include <stdio.h>
# include <stdint.h>

# ifdef __cplusplus
extern "C" {
# endif

/** Global logging file descriptor for DPhMC lib. Set to NULL initially
 * indicating output to stdout.
 **/
extern FILE * G_DPhMC_msgstream;

/** Global error file descriptor for DPhMC lib. Set to NULL initially
 * indicating output to stderr.
 **/
extern FILE * G_DPhMC_errstream;

/** By default set to 1. Messages with greater level won't be printed.
 **/
extern signed char G_DPhMC_verbosity;

/** Performs log output. For verbose messages levels 1-3 are reserved. Warning
 * message corresponds to -1, error -- -2. 0 causes unconditional output to 
 * std-logstream.
 **/
void DPhMC_msg( int level, const char * fmtstr, ... );

# define DPhMC_msg1( ... ) DPhMC_msg( 1, __VA_ARGS__ )
# define DPhMC_msg2( ... ) DPhMC_msg( 2, __VA_ARGS__ )
# define DPhMC_msg3( ... ) DPhMC_msg( 3, __VA_ARGS__ )

# define DPhMC_msgw( ... ) DPhMC_msg( -1, __VA_ARGS__ )
# define DPhMC_msge( ... ) DPhMC_msg( -2, __VA_ARGS__ )

/*
 * Error handling
 */

# define for_all_DPhMC_error_codes(m)                                         \
    m(0x1,   common,            "Common DPhMC lib error."                   ) \
    m(0x2,   badArchitect,      "Unexpected component state."               ) \
    m(0x3,   badState,          "Unexpected FSM state."                     ) \
    m(0x4,   badCast,           "Types mismatch."                           ) \
    m(0x5,   badValue,          "Bad value revealed."                       ) \
    m(0x6,   badParameter,      "Bad parameter or parameter set provided."  ) \
    m(0x7,   notFound,          "Data could not be found."                  ) \
    m(0x8,   thirdParty,        "Third-party code error."                   ) \
    m(0xf,   unimplemented,     "The code reached is not yet implemented."  ) \
    /* ... */

typedef uint8_t DPhMC_ErrorCode;

# define declare_error_code(code, name, description) \
                    extern const DPhMC_ErrorCode DPhMC_E_ ## name;
for_all_DPhMC_error_codes(declare_error_code)
# undef declare_error_code

/** Throws an exception with given details. Useful for C++ error reporting
 * from C.
 */
void DPhMC_error( DPhMC_ErrorCode, const char * fmtstr, ... );

# ifdef __cplusplus
}  // extern "C"
# endif

void DPhMC_C_default_error_handle(
        DPhMC_ErrorCode code,
        const char * ) __attribute__ ((noreturn));

# endif  // H_DPHMC_LOGGING_H

