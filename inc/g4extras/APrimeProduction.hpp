/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_DPHMC_APRIME_HADRONIC_INTERACTION_H
# define H_DPHMC_APRIME_HADRONIC_INTERACTION_H

# include "dphmc-config.h"

# ifdef AMIXING_PROCESS_USES_HADRONIC_API

# include <G4HadronicInteraction.hh>

namespace DPhMC {

/**@brief Final state production model for A' mixin production.
 * @class APrimeProduction
 *
 * This hadronic interaction class should be complementary used
 * with A' mixing process implementation (composed, in order, as
 * a subclass of G4HadronicProcess).
 *
 * It should be choosen by theEnergyRangeManager attribute of
 * G4HadronicProcess instance (which is, in order, an object
 * of class G4EnergyRangeManager unique for each process type(?)).
 */
class APrimeProduction : public G4HadronicInteraction {
public:

    APrimeProduction();

    ~APrimeProduction();

    /// Sets the appropriate final states for generated A' particle
    /// and incident charged projectile.
    virtual G4HadFinalState * ApplyYourself( const G4HadProjectile& aTrack, 
                                             G4Nucleus& aTargetNucleus ) override;
};

}  // namespace DPhMC

# endif  // AMIXING_PROCESS_USES_HADRONIC_API

# endif  // H_DPHMC_APRIME_HADRONIC_INTERACTION_H

