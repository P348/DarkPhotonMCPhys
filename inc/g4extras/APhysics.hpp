/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_DPHMC_CUSTOM_PHYSICS_APRHYSICS_H
# define H_DPHMC_CUSTOM_PHYSICS_APRHYSICS_H

/**@file APhysics.hpp
 * @brief A prime physics class header.
 *
 * PRovides declaration of APrimePhysics class.
 **/

# include "dphmc-config.h"

# include <G4VPhysicsConstructor.hh>

# include <sstream>
# include <set>

// fwds:
class TRandom;
class G4VPhysicsConstructor;

namespace DPhMC {

G4VPhysicsConstructor * construct_aprime_physics(TRandom * rndg=NULL);

/**@brief Geant4 physics module.
 *
 * Provides dynamic association of A' physics with charged
 * particles found in current model.
 *
 * matMinAWWApprox is the minimal atomic number where  WW approximation
 * from \cite{Bjorken} is valid.
 */
class APrimePhysics : public G4VPhysicsConstructor {
private:
    int _verbosityLevel;
    std::stringstream _ss;

    const double _aPrimeMass,
                 _mixingConstant;
    TRandom * _rndGen;
    const uint8_t _matMinAWWApprox;
    const bool _decayEnabled;

    /// A public ptr to process instance. Ptr is useful for querying.
    static G4VProcess * _mixingProcessInstancePtr;
public:
    APrimePhysics( int verbosity,
                   double aprimeMass,
                   double aprimeMixingParameter,
                   TRandom * rndGenerator,
                   const std::set<double> & energies,
                   uint8_t matMinAWWApprox,
                   bool enableAPrimeDecay );
    ~APrimePhysics();

    virtual void ConstructParticle() override;
    virtual void ConstructProcess() override;

    double a_prime_mass() const { return _aPrimeMass; }
    double mixing_constant() const { return _mixingConstant; }
    const TRandom * random_generator() const { return _rndGen; }
    bool is_decay_enabled() const { return _decayEnabled; }

    /// Returns True once process was constructed.
    static const G4VProcess * process() { return _mixingProcessInstancePtr; }
};  // class APrimePhysics

}  // namespace DPhMC

# endif  // H_DPHMC_CUSTOM_PHYSICS_APRHYSICS_H

