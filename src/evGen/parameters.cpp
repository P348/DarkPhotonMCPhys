/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "dphmc-config.h"

# include <CLHEP/Units/PhysicalConstants.h>

# include "evGen/parameters.h"

//
// C++ part of DPhMClib-constants.
//

namespace DPhMC {
namespace aux {

extern const double electronMass_GeV = CLHEP::electron_mass_c2 / CLHEP::GeV,
                    protonMass_GeV = CLHEP::proton_mass_c2 / CLHEP::GeV,
                    muProton = 2.79,
                    fineStructureConstant = CLHEP::fine_structure_const,
                    // GeV^(-2) = 0.3894 mb = 0.3894 * 1e9 pb =
                    pbarn_per_GeV = 3.894e+8;

}  // namespace aux
}  // namespace DPhMC

// C
# define assign( type, name, descr ) extern const type _DPhMC_CST_ ## name = DPhMC::aux:: name;
for_all_PhMClib_numerical_constants(assign)
# undef assign

