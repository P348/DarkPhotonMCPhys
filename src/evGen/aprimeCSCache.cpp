/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "dphmc-config.h"
# include "evGen/aprimeCSCache.hpp"
# include <G4Electron.hh>

namespace DPhMC {

APrimeCSCaches * APrimeCSCaches::_self = nullptr;
double APrimeCSCaches::APrimeMass = 0.;
double APrimeCSCaches::MixingConstant = 0.;
double APrimeCSCaches::CSMultFactor = 1;

APrimeCSCaches & APrimeCSCaches::self() {
    if( APrimeCSCaches::_self ) {
        return *APrimeCSCaches::_self;
    }
    return *( _self = new APrimeCSCaches() );
}

APrimeCSCaches::APrimeCSCaches() : _rndGenerator(nullptr) {
    # ifndef STANDALONE_BUILD
    assert( APrimeMass );
    assert( APrimeMass > 0 );
    assert( MixingConstant );
    assert( MixingConstant > 0 );
    # endif
}

void
APrimeCSCaches::_clear() {
    // Delete generator pointers from caches:
    for( auto it = _caches.begin(); _caches.end() != it; ++it ) {
        _delete_generator( it->generator );
    }
    _caches.clear();
    _projectileERanges.clear();
}

APrimeGenerator *
APrimeCSCaches::_new_generator( double q, double m, double E, uint16_t A, uint8_t Z ) {
    # ifdef STANDALONE_BUILD
    return new APrimeGenerator(q, m, E, Z);
    # else
    assert( self()._rndGenerator );  // check, if random generator was set
    // todo: for other than (e-) particles support,
    // we will need to tune APrimeGenerator using more
    // parameters, so here we must check m, q.
    # ifndef NDEBUG
    {
        const double misThreshold = 1e-6,
                     refPMass = std::fabs(G4Electron::Definition()->GetPDGMass()),
                     refPCharge = std::fabs(G4Electron::Definition()->GetPDGCharge()),
                     deltaMass = std::abs(refPMass - std::fabs(m))
                                /std::abs(refPMass + std::fabs(m)),
                     deltaCharge = std::abs(refPCharge - std::fabs(q))
                                  /std::abs(refPCharge + std::fabs(q))
                    ;
        if( deltaMass > misThreshold || deltaCharge > misThreshold ) {
            DPhMC_exception_throw( unimplemented,
                "APrimeGenerator creation invoked for particle which "
                "not not seems to be e+/e-: mass=%e, charge=%e, while "
                "mass_{e-}=%e, charge_{e-}=%e.",
                m, q, G4Electron::Definition()->GetPDGMass(),
                G4Electron::Definition()->GetPDGCharge() );
        }
    }
    # endif  // NDEBUG
    char bf[256];
    snprintf( bf, sizeof(bf),
             "APrimeGenerator:m%d,%d;%e;%e,%e",
             (int) Z, (int) A,
             E, m, q );
    APrimeGenerator * newG = new APrimeGenerator(
            bf,
            //for_all_PhMClib_aprimeCS_parameters:
            /* Z ......... */ Z,
            /* A ......... */ A,
            /* massA_GeV . */ APrimeMass,
            /* EBeam_GeV . */ E,
            /* epsilon ... */ MixingConstant,
            /* factor .... */ CSMultFactor,
            //for_all_PhMClib_aprimeCS_GSL_chi_comp_parameter:
            # define obtain_numsparameter_arg( type, txtName, dft, name, descr ) \
                Config::instance().option_as<type>(txtName),
                    for_all_PhMClib_aprimeCS_GSL_chi_comp_parameter( obtain_numsparameter_arg )
            # undef obtain_numsparameter_arg
            /* epsabs .... */
            /* epsrel .... */
            /* erelst .... */
            /* limit ..... */
            /* nnodes .... */
            # define obtain_tfoam_parameter( type, txtName, dft, name, descr ) \
                Config::instance().option_as<type>(txtName),
                    for_all_PhMClib_TFoam_generator_parameters( obtain_tfoam_parameter )
            # undef obtain_tfoam_parameter
            nullptr
        );
    newG->initialize( self()._rndGenerator );
    return newG;
    # endif
}

void
APrimeCSCaches::_delete_generator( APrimeGenerator * g ) {
    # ifdef STANDALONE_BUILD
    delete g;
    # else
    delete g; // whatever?
    # endif
}

void
APrimeCSCaches::random_generator( TRandom * rndG ) {
    if( _rndGenerator ) {
        for( auto it = _caches.begin(); _caches.end() != it; ++it ) {
            it->generator->initialize( rndG );
        }
    }
    _rndGenerator = rndG;
}

APrimeGenerator *
APrimeCSCaches::generator(
                            double incidentCharge,
                            double incidentMass,
                            double incidentE,
                            uint16_t nucleusA,
                            uint8_t nucleusZ ) {
    # ifndef NDEBUG
    if( !incidentCharge || std::isnan( incidentCharge )
     || !incidentMass   || std::isnan( incidentMass )
     || !incidentE      || std::isnan( incidentE )
     || !nucleusZ
     || !nucleusA ) {
        DPhMC_exception_throw( /*assertFailed*/ badState, "Bad conditions for APrimeCSCaches: q=%e, m=%e,"
                 " E_proj=%e, Z_mat=%u A_mat=%u.", incidentCharge, incidentMass,
                 incidentE, nucleusZ, nucleusA );
    }
    # endif

    // This assertion failure indicates that tabulation was not set
    // up to the moment of generator() method invokation:
    assert(! _projectileERanges.empty() );

    // Find nearest tabulation boundary:
    double nearestProjE; {
        std::set<double>::const_iterator itUp = _projectileERanges.upper_bound( incidentE );
        if( _projectileERanges.end() == itUp &&
            incidentE < *_projectileERanges.crbegin() + 1e-12 &&
            incidentE > *_projectileERanges.crbegin() - 1e-12 ) {
            --itUp;
        }
        # ifndef NDEBUG
        if( _projectileERanges.end() == itUp ) {
            DPhMC_exception_throw( /*assertFailed*/ badState, "Projectile energy E_proj=%e>=%e "
                 "lays above of expected range.", incidentE, *_projectileERanges.crbegin() );
        }
        if( _projectileERanges.begin() == itUp ) {
            DPhMC_exception_throw( /*assertFailed*/ badState, "Projectile energy E_proj=%e<%e "
                 "lays below of expected range.", incidentE, *_projectileERanges.cbegin() );
        }
        # endif
        std::set<double>::const_iterator itLow = itUp; --itLow;
        const double lowDistance = incidentE - *itLow,
                     upDistance  = *itUp - incidentE;
        nearestProjE = lowDistance > upDistance ? *itUp : * itLow;
    }

    CacheContainer::const_iterator it = _caches.find(
            std::make_tuple( incidentCharge, incidentMass, nearestProjE, nucleusA, nucleusZ ) );
    if( _caches.end() == it ) {
        auto insertionResult = _caches.insert({
                _new_generator( incidentCharge, incidentMass, nearestProjE, nucleusA, nucleusZ ),
                                incidentCharge, incidentMass, nucleusA, nucleusZ, nearestProjE
            });
        if( !insertionResult.second ) {
            DPhMC_exception_throw( badArchitect, "Element insertion failure! "
                     "Something went wrong with caches look-up procedures "
                     "when existing generator was searched with conditions: "
                     "q=%e, m=%e, E_proj=%e, Z_mat=%u, A_mat = %u",
                     incidentCharge, incidentMass, incidentE, nucleusZ, nucleusA );
        }
        # ifndef STANDALONE_BUILD
        DPhMC_msg2( "New A' event generator instance cached: "
                     "q=%e, m=%e, E_proj=%e (nearest %e), Z_mat=%u, A_mat = %u.\n",
                     incidentCharge, incidentMass, incidentE, nearestProjE, nucleusZ, nucleusA );
        # endif
        it = insertionResult.first;
    }
    return it->generator;
}

//
//
//

# ifdef STANDALONE_BUILD

// TODO: support for material A number

int
APrimeCSCaches::test_indexing_routines( int seed, size_t nIterations, std::ostream & os ) {
    srand( seed );
    // Note: energyTabulation must be sorted!
    const double energyTabulation[] = { 50, 100, 200, 400, 800, 1600 };
    const double charges[] = { 1, -1 },
                 masses[] = { 1, 2, 3, 5 }
                 ;
    const uint8_t Zs[] = { 1, 15, 26 }
                 ;

    // Set tabulation ranges:
    APrimeCSCaches::self().projectile_energy_tabulation(
            std::set<double>( energyTabulation,
                              energyTabulation + sizeof(energyTabulation)/sizeof(energyTabulation[0]) ) );

    // Run generators acuizition:
    os << "In order to test caching mechanics, run log will "
          "be performed following graph notation:" << std::endl
       << "    \"+\\n\" means that new generator created for some conditions;" << std::endl
       << "    \".\" means that existing generator is obtained for usage." << std::endl
       << "It is expected that one will observe decreasing number "
          "of \"+\"'es up to the finish of the run."
       << std::endl;
    std::set<const APrimeGenerator *> uniqGenerators;
    # define array_size( array ) ( sizeof(array)/sizeof(array[0]) )
    # define random_element( array ) \
    ( array[ (size_t) std::round( (double(rand())/RAND_MAX) * (array_size(array) - 1) ) ] )
    for( size_t n = 0; n < nIterations; ++n ) {
        double charge,
               mass,
               E;
        uint8_t Z;
        auto g = APrimeCSCaches::self().generator(
                        charge = random_element( charges ),
                        mass = random_element( masses ),
                        E = *APrimeCSCaches::self().projectile_energy_tabulation().cbegin() +
                        (double(rand())/RAND_MAX)*(
                            *APrimeCSCaches::self().projectile_energy_tabulation().crbegin()
                            - *APrimeCSCaches::self().projectile_energy_tabulation().cbegin()),
                        107,
                        Z = random_element( Zs ) );
        if( g->q != charge ) { return -1; }
        if( g->m != mass ) {   return -2; }
        if( g->Z != Z ) {      return -3; }
        { //assert( g.E == E ); --- find nearest E_inc:
            const double * lowE = std::lower_bound( energyTabulation,
                                                    energyTabulation + array_size(energyTabulation),
                                                    E );
            assert( *lowE >= E );  // by the sense of std::lower_bound(...)
            const double downDistance = std::fabs( *lowE - E );
            if( lowE == energyTabulation + array_size(energyTabulation) ) {
                return -4;  // Actually, means that energy was generated wrong.
            }
            if( lowE == energyTabulation && *energyTabulation != E ) {
                return -5;   // Actually, means that energy was generated wrong.
            }
            if( *energyTabulation != E ) {  // otherwise almost impossible, however...
                const double upDistance = std::fabs( *(lowE - 1) - E );
                double nearest;
                if( upDistance > downDistance ) {
                    nearest = *lowE;
                } else {
                    nearest = *(lowE - 1);
                }
                if( nearest != g->E ) {
                    return -6;
                }
            }
        }
        if( uniqGenerators.find( g ) == uniqGenerators.end() ) {
            os << "+" << std::endl;
            uniqGenerators.insert(g);
        } else {
            os << ".";
        }
    }
    os << std::endl << "Done!" << std::endl;
    # undef random_element
    {
        size_t maxGenerators = array_size( energyTabulation ) *
                               array_size( charges ) *
                               array_size( masses ) *
                               array_size( Zs )
                               ;
        os << "Generators created during run: "
                  << APrimeCSCaches::self().n_generators() << std::endl;
        os << "    while at most "
                  << maxGenerators << " generators could be created." << std::endl;
        return maxGenerators - APrimeCSCaches::self().n_generators();
    }
    # undef array_size
}
# endif  // STANDALONE_BUILD

}  // namespace DPhMC

# ifdef STANDALONE_BUILD
int
main( int argc, char * argv[] ) {
    int rc;
    if( (rc = DPhMC::APrimeCSCaches::test_indexing_routines()) != 0 ) {
        if( rc < 0 ) {
            std::cerr << "Something wrong (" << rc << ")." << std::endl;
            return EXIT_FAILURE;
        }
        std::cerr << "Generators missed: " << rc << std::endl;
    }
    return EXIT_SUCCESS;
}
# endif

