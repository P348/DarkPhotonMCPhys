/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "dphmc-config.h"

# include <cassert>
# include <cmath>

# include <TMath.h>
# include <Math/IFunction.h>

//# pragma GCC diagnostic push
//# pragma GCC diagnostic ignored "-Wdeprecated-declarations"  // "auto_ptr" is depr-d
# include <Math/IntegratorMultiDim.h>
# include <Math/Functor.h>
//# pragma GCC diagnostic pop

# include <TH2F.h>

# include <CLHEP/Units/GlobalSystemOfUnits.h>

# include "evGen/parameters.h"
# include "evGen/aprimeEvGen.hpp"

namespace DPhMC {

APrimeWWPDF::Conditions APrimeWWPDF::_conditions = APrimeWWPDF::Conditions();

//
// WW-method PDF wrapper for C according to \cite{JDBjorken}
//

double
APrimeWWPDF::_x_bound( uint8_t n ) const {
    assert( n < 2 );
    return ( 1 > n ? lower_cut_x( _cParameters ) : upper_cut_x( _cParameters ) );
}

double
APrimeWWPDF::_theta_max() const {
    return upper_cut_theta(_cParameters);
}

APrimeWWPDF::APrimeWWPDF() :
                    _cachesValid(false),
                    # define set_physparameter_arg( type, txtName, name, descr ) \
                            _cache_ ## name ## _set(false),
                        for_all_PhMClib_aprimeCS_parameters( set_physparameter_arg )
                    # undef set_physparameter_arg
                    # define set_numsparameter_arg( type, txtName, dft, name, descr ) \
                            _cache_ ## name(dft),
                        for_all_PhMClib_aprimeCS_GSL_chi_comp_parameter( set_numsparameter_arg )
                    # undef set_numsparameter_arg
                    _cParameters(NULL), _integrdCache(NAN) { }

APrimeWWPDF::APrimeWWPDF(
            # define declare_physparameter_arg( type, txtName, name, descr ) type name ## _,
                for_all_PhMClib_aprimeCS_parameters( declare_physparameter_arg )
            # undef declare_physparameter_arg
            # define declare_numsparameter_arg( type, txtName, dft, name, descr ) type name ## _,
                for_all_PhMClib_aprimeCS_GSL_chi_comp_parameter( declare_numsparameter_arg )
            # undef declare_numsparameter_arg
            void *
    ) :
                    _cachesValid(false),
                    # define set_physparameter_arg( type, txtName, name, descr ) \
                            _cache_ ## name(name ## _), \
                            _cache_ ## name ## _set(true),
                        for_all_PhMClib_aprimeCS_parameters( set_physparameter_arg )
                    # undef set_physparameter_arg
                    # define set_numsparameter_arg( type, txtName, dft, name, descr ) \
                            _cache_ ## name(name ## _),
                        for_all_PhMClib_aprimeCS_GSL_chi_comp_parameter( set_numsparameter_arg )
                    # undef set_numsparameter_arg
                    _cParameters(NULL), _integrdCache(NAN) {
    //const double mins[] = {      _x_bound( 0),
    //                            -_theta_max() },
    //             maxs[] = { 1. - _x_bound( 1),
    //                             _theta_max() };
    //XForm::set_ranges(mins, maxs);
    //recache();
}

APrimeWWPDF::APrimeWWPDF( const APrimeWWPDF & o ) :
                    XForm(),
                    _cachesValid(false),
                    # define set_physparameter_arg( type, txtName, name, descr )    \
                            _cache_ ## name        (o._cache_ ## name ),            \
                            _cache_ ## name ## _set(o._cache_ ## name ## _set),
                        for_all_PhMClib_aprimeCS_parameters( set_physparameter_arg )
                    # undef set_physparameter_arg
                    # define set_numsparameter_arg( type, txtName, dft, name, descr ) \
                            _cache_ ## name(o._cache_ ## name),
                        for_all_PhMClib_aprimeCS_GSL_chi_comp_parameter( set_numsparameter_arg )
                    # undef set_numsparameter_arg
                    _cParameters(NULL), _integrdCache(NAN) {
    //const double mins[] = {      _x_bound( 0),
    //                            -_theta_max() },
    //             maxs[] = { 1. - _x_bound( 1),
    //                             _theta_max() };
    //XForm::set_ranges(mins, maxs);
    // recache() ?
}

APrimeWWPDF::~APrimeWWPDF() {
    _free_C_workspace();
}


bool
APrimeWWPDF::is_complete() const {
    if(
        # define check_physparameter_arg( type, txtName, name, descr ) \
            _cache_ ## name ## _set &&
            for_all_PhMClib_aprimeCS_parameters( check_physparameter_arg )
        # undef check_physparameter_arg
        true
    ) {
        return true;
    }
    return false;
}

void
APrimeWWPDF::recache() const {
    DPhMC_msg3( "APrimeWWPDF functor recaching routine invoked (EBeam=%e).\n", _cache_EBeam_GeV );
    // # ifndef NDEBUG
    if( !is_complete() ) {
        json_dump( std::cerr );
        DPhMC_exception_throw( badState, "Couldn't evaluate APrimeWWPDF calculation due to incomplete conditions." );
    }
    // # endif
    init_aprime_cross_section_workspace(
                    # define send_physparameter_arg( type, txtName, name, descr ) \
                            _cache_ ## name,
                        for_all_PhMClib_aprimeCS_parameters( send_physparameter_arg )
                    # undef send_physparameter_arg
                    # define send_numsparameter_arg( type, txtName, dft, name, descr ) \
                            _cache_ ## name,
                        for_all_PhMClib_aprimeCS_GSL_chi_comp_parameter( send_numsparameter_arg )
                    # undef send_numsparameter_arg
                    &_cParameters
    );
    const double mins[] = {  _x_bound( 0),
                             0 },
                 maxs[] = {  _x_bound( 1),
                             _theta_max() };
    const_cast<XForm *>(static_cast<const XForm *>(this))->set_ranges(mins, maxs);
    _cachesValid = true;
}

void
APrimeWWPDF::_free_C_workspace() {
    ::free_aprime_cross_section_workspace( _cParameters );
    _cParameters = NULL;
}

double
APrimeWWPDF::integral_sum_a15() const {
    recache_if_need();
    return ::analytic_integral_estimation( _cParameters );
}

std::pair<double, double>
APrimeWWPDF::x_ranges() const {
    recache_if_need();
    return {_x_bound( 0 ),
            _x_bound( 1 )};
}

void
APrimeWWPDF::add_cut_off( CutOffCallback c ) {
    _conditions.add_condition(c);
}

/** This method, comparing to aprime_cross_section_a12() function reterns the following result:
 * \f[
 * \frac{ d \sigma_{ 2 \rightarrow 3 } }{ d x d \theta_{A'} }
 * \f]
 * Note the difference between this two formulas: \f$1/(E_{0}^2 x)\f$ factor and
 * \f$d \cos{\theta{A'} = -sin{\theta_{A'}} d \theta_{A'} }\f$
 */
double
APrimeWWPDF::density( double x, double theta ) const {
    recache_if_need();
    if( !_conditions.every( { (void*) _cParameters, x, theta } ) ) {
        //std::cerr << "condition(s) failed!" << std::endl;  // XXX
        return 0.;
    }
    if(     x < XForm::get_limit( XForm::min, 0 ) ||     x > XForm::get_limit( XForm::max, 0 ) ||
        theta < XForm::get_limit( XForm::min, 1 ) || theta > XForm::get_limit( XForm::max, 1 ) ) {
        DPhMC_msgw( "Point [x=%e, theta=%e] lies outside of validity region for %p: "
                    "(%e < x < %e), (%e < theta < %e).\n",
                    x, theta, (void *) this,
                    XForm::get_limit( XForm::min, 0 ),
                    XForm::get_limit( XForm::max, 0 ),
                    XForm::get_limit( XForm::min, 1 ),
                    XForm::get_limit( XForm::max, 1 ) );
    }
    assert( _cParameters );
    double res = aprime_cross_section_a12( x, theta, _cParameters );
    
    {
        const double & E0 = _cache_EBeam_GeV;
        res *= E0*E0*x*sin(theta);
    }

    # ifndef APRIME_PDF__NO_STATS_CHECK
    double checkSet[3] = { x, theta, res };
    _check1.update_ranges( checkSet );
    # endif

    return res;
}

double
APrimeWWPDF::probability( double xLow,     double xUp,
                          double thetaLow, double thetaUp ) const {
    recache_if_need();
    if( xLow < 0 && std::isinf(xLow) ) {
        xLow = XForm::get_limit( XForm::min, 0 );
    }
    if( xUp > 0 && std::isinf(xUp) ) {
        xUp = XForm::get_limit( XForm::max, 0 );
    }
    if( thetaLow < 0 && std::isinf(thetaLow) ) {
        thetaLow = XForm::get_limit( XForm::min, 1 );
    }
    if( thetaUp > 0 && std::isinf(thetaUp) ) {
        thetaUp = XForm::get_limit( XForm::max, 1 );
    }
	ROOT::Math::Functor Func( ReferencingProxyDCos(*this), 2 );
	ROOT::Math::IntegratorMultiDim Integrator( Func );
    const double mins[] = {xLow, thetaLow},
                 maxs[] = {xUp,  thetaUp}
                 ;
	double integral = Integrator.Integral( mins, maxs );
    return integral;
}

# if 0
double  // XXX:
APrimeWWPDF::integral_XXX() const {
    recache_if_need();
	ROOT::Math::Functor Func( ReferencingProxyByTheta(*this), 2 );
	ROOT::Math::IntegratorMultiDim Integrator( Func );
    const double mins[] = {get_limit( DPhMC::aux::XForm<2, Double_t>::min, 0 ),
                           get_limit( DPhMC::aux::XForm<2, Double_t>::min, 1 ) },
                 maxs[] = {get_limit( DPhMC::aux::XForm<2, Double_t>::max, 0 ),
                           get_limit( DPhMC::aux::XForm<2, Double_t>::max, 1 ) }
                 ;
	double integral = Integrator.Integral( mins, maxs );
    return integral;
}
# endif

//double
//APrimeWWPDF::max_density() const {
//    recache_if_need();
//    return aprime_cs_at_border( _cParameters );
//}

void
APrimeWWPDF::json_dump( std::ostream & os ) const {
    os  << "{"
        << " class : \"APrimeWWPDF\"," << std::endl
        << " pointer : " << (void *) this << "," << std::endl;
        # define print_physparameter_arg( type, txtName, name, descr ) \
                os << " " << txtName << " : "; \
                if( _cache_ ## name ## _set) { os << "<unset>"; } else { os << ", "; } \
                os << std::endl;
            for_all_PhMClib_aprimeCS_parameters( print_physparameter_arg )
        # undef print_physparameter_arg
        os
        # define print_numsparameter_arg( type, txtName, dft, name, descr ) \
                << " " << txtName << " : " << _cache_ ## name << ", " << std::endl
            for_all_PhMClib_aprimeCS_GSL_chi_comp_parameter( print_numsparameter_arg )
        # undef print_numsparameter_arg
            << " CWorkspacePointer : " << _cParameters << ";" << std::endl;
    os << "}";
}

//
// Distribution Implementation
//

Double_t
APrimeWWPDF::Density(int nDim, Double_t * Xarg) {
    recache_if_need();
    assert(2 == nDim);
    if( !ranges_set() ) {
        DPhMC_exception_throw( badState, "Ranges are unset for APrimeWWPDF object %p.", (void *) this );
    }
    # ifdef APRIME_PDF__NO_STATS_CHECK
    double renormed[2] = {Xarg[0], Xarg[1]};
    XForm::renorm( renormed );
    return density( renormed[0], renormed[1] );
    # else
    double renormed[2] = {Xarg[0], Xarg[1]},
           checkSet[3] = {Xarg[0], Xarg[1], NAN};
    XForm::renorm( renormed );
    checkSet[2] = density( renormed[0], renormed[1] );
    _check2.update_ranges( checkSet );
    return checkSet[2];
    # endif
}

double
APrimeWWPDF::ReferencingProxyDCos::DoEval( const double * x ) const {
    //return const_cast<APrimeWWPDF*>(this)->Density( 2, const_cast<double *>(x) );
    return _instance.density( x[0], x[1] );
}

ROOT::Math::IBaseFunctionMultiDim *
APrimeWWPDF::ReferencingProxyDCos::Clone() const {
    return new APrimeWWPDF::ReferencingProxyDCos( this->_instance );
}

# if 0
// XXX:
double
APrimeWWPDF::ReferencingProxyByTheta::DoEval( const double * x ) const {
    //return const_cast<APrimeWWPDF*>(this)->Density( 2, const_cast<double *>(x) );
    double a = _instance.density( x[0], x[1] )*sin(x[1]);
    if( !a ) {
        std::cout << "x = " << x[0] << ", theta = " << x[1]
                  << ", density = " << _instance.density( x[0], x[1] )
                  << ", sin(theta) = " << sin(x[1])
                  << std::endl
                  ;
    }
    return a;
}

ROOT::Math::IBaseFunctionMultiDim *
APrimeWWPDF::ReferencingProxyByTheta::Clone() const {
    return new APrimeWWPDF::ReferencingProxyByTheta( this->_instance );
}
// XXX^^^
# endif

double
APrimeWWPDF::convert_cross_section_units_to_CLHEP_units( double csv_GeVmp2 ) {
    return ( csv_GeVmp2 * ::DPhMC::aux::pbarn_per_GeV * CLHEP::picobarn );
}

//
// Generator
//

APrimeGenerator::APrimeGenerator(
            const std::string & name,
            # define declare_physparameter_arg( type, txtName, name, descr ) type name,
                for_all_PhMClib_aprimeCS_parameters( declare_physparameter_arg )
            # undef declare_physparameter_arg
            # define declare_numsparameter_arg( type, txtName, dft, name, descr ) type name,
                for_all_PhMClib_aprimeCS_GSL_chi_comp_parameter( declare_numsparameter_arg )
            # undef declare_numsparameter_arg
            # define declare_tfoam_parameter( type, txtName, dft, name, descr ) type name,
                for_all_PhMClib_TFoam_generator_parameters( declare_tfoam_parameter )
            # undef declare_tfoam_parameter
            void *
            ) :
    Parent(
        _integrandInstancePtr = new APrimeWWPDF(
            # define set_physparameter_arg( type, txtName, name, descr ) name,
                for_all_PhMClib_aprimeCS_parameters( set_physparameter_arg )
            # undef set_physparameter_arg
            # define set_numsparameter_arg( type, txtName, dft, name, descr ) name,
                for_all_PhMClib_aprimeCS_GSL_chi_comp_parameter( set_numsparameter_arg )
            # undef set_numsparameter_arg
            nullptr
        ),
        name,
        # define set_tfoam_parameter( type, txtName, dft, name, descr ) name,
            for_all_PhMClib_TFoam_generator_parameters( set_tfoam_parameter )
        # undef set_tfoam_parameter
        nullptr
        ),
    _externalIntegrandInstance(false),
    _isInitialized(false), _eventGenerated(false) { }

APrimeGenerator::APrimeGenerator(
            const std::string & name,
            APrimeWWPDF * gPtr,
            # define declare_tfoam_parameter( type, txtName, dft, name, descr ) type name,
                for_all_PhMClib_TFoam_generator_parameters( declare_tfoam_parameter )
            # undef declare_tfoam_parameter
            void *
            ) :
    Parent(
        gPtr,
        name,
        # define set_tfoam_parameter( type, txtName, dft, name, descr ) name,
            for_all_PhMClib_TFoam_generator_parameters( set_tfoam_parameter )
        # undef set_tfoam_parameter
        nullptr
        ),
    _integrandInstancePtr(gPtr),
    _externalIntegrandInstance(true),
    _isInitialized(false), _eventGenerated(false) { }

APrimeGenerator::~APrimeGenerator(){
    // todo: causes ROOT get double free at any damn case!
    //if( _externalIntegrandInstance ) {
    //    delete _integrandInstancePtr;
    //}
    (void)(_externalIntegrandInstance);  // Supress `not used' warning
}

APrimeWWPDF &
APrimeGenerator::integrand() {
    assert( _integrandInstancePtr );
    return *_integrandInstancePtr;
}

const APrimeWWPDF &
APrimeGenerator::integrand() const {
    assert( _integrandInstancePtr );
    return *_integrandInstancePtr;
}

void
APrimeGenerator::initialize( TRandom * rndGen ) {
    _integrandInstancePtr->recache();
    Parent::initialize( rndGen );
    _isInitialized = true;
    assert( _integrandInstancePtr->cache_is_valid() );
}

double
APrimeGenerator::generate( Event & e ) const {
    assert( _isInitialized );
    if( !_integrandInstancePtr->cache_is_valid() ) {
        DPhMC_exception_throw( badState,
                 "Integrand instance %p was changed after generator %p initialization.",
                 (void *) _integrandInstancePtr,
                 (void *) this );
    }
    double w;
    # ifdef APRIME_PDF__NO_STATS_CHECK
    w = Parent::generate(e);
    _integrandInstancePtr->renorm( e.x );
    # else
    Double_t checkSet[] = {
            0., 0., Parent::generate(e)
        };
    checkSet[0] = e.x[0];
    checkSet[1] = e.x[1];
    _integrandInstancePtr->renorm( e.x );
    _integrandInstancePtr->renorm( checkSet );
    _integrandInstancePtr->_check3.update_ranges( checkSet );
    w = checkSet[2];
    # endif

    # ifndef APRIME_PDF__NO_EVENTS_STORAGING
    _generatedEventsHistory.push_back( e );
    # endif

    _eventGenerated = true;
    return w;
}

double
APrimeGenerator::integral_sum() const {
    assert( _isInitialized );
    if( !event_generated() ) {
        DPhMC_msgw( "No event(s) was generated up to "
                     "APrimeGenerator::integral_sum() was invoked. "
                     "Forcing TFoam MC-integration by generating first event. "
                     "It is recommended to call PDF's probability() method instead.\n");
        Event dummyE;
        this->generate( dummyE );
    }
    integrand().recache_if_need();
    double pdfIntVal, intErrRel;
    const_cast<APrimeGenerator*>(this)->GetIntNorm( pdfIntVal, intErrRel );
    //pdfIntVal /= integrand().scales()[0]*integrand().scales()[1];
    // TODO: (27/09/016) since we have changed XForm some time ago and for now I'm
    // not pretty sure whetherer scales() -> width()
    pdfIntVal /= integrand().range(0).range_width()*integrand().range(1).range_width();
    return pdfIntVal;
}

# ifndef APRIME_PDF__NO_EVENTS_STORAGING
TH2F *
APrimeGenerator::draw_agreement_plot( uint8_t nBinsX, uint8_t nBinsTheta ) const {
    if( _generatedEventsHistory.empty() ) {
        DPhMC_msgw( "Events history of APrimeGenerator %p is empty."
                     "Refusing to build the agreement plot.", this );
        return nullptr;
    }

    TH2F * eventsHst = new TH2F( "generatedEvents", "Test events",
                nBinsX,
                integrand().get_limit( DPhMC::APrimeWWPDF::XForm::min, 0 ),
                integrand().get_limit( DPhMC::APrimeWWPDF::XForm::max, 0 ),
                nBinsTheta,
                integrand().get_limit( DPhMC::APrimeWWPDF::XForm::min, 1 ),
                integrand().get_limit( DPhMC::APrimeWWPDF::XForm::max, 1 )
            );
    for( auto it  = _generatedEventsHistory.cbegin();
         _generatedEventsHistory.cend() != it; ++it ) {
        eventsHst->Fill( it->byName.x, it->byName.theta );
    }

    TH2F * normalized = dynamic_cast<TH2F*>( eventsHst->DrawNormalized( "COLZ" ) );

    // as we use integrand() function normed here, and pdfIntVal sum is computed
    // through the TFoam() facility:
    const double pdfIntVal = integral_sum();
    double sumEmpir = 0.,
           sumTheor = 0.;
    for( size_t nBinX = 1; nBinX <= nBinsX; ++nBinX ) {
        for( size_t nBinTheta = 1; nBinTheta <= nBinsTheta; ++nBinTheta ) {
            const double binEdgesX[2] = {
                    normalized->GetXaxis()->GetBinLowEdge( nBinX ),
                    normalized->GetXaxis()->GetBinLowEdge( nBinX ) + normalized->GetXaxis()->GetBinWidth( nBinX )
                },
                binEdgesTheta[2] = {
                    normalized->GetYaxis()->GetBinLowEdge( nBinTheta ),
                    normalized->GetYaxis()->GetBinLowEdge( nBinTheta ) + normalized->GetYaxis()->GetBinWidth( nBinTheta )
                };
            const double empiricPDF = normalized->GetBinContent( nBinX, nBinTheta ),
                         theorPDF = integrand().probability(
                                binEdgesX[0],       binEdgesX[1],
                                binEdgesTheta[0],   binEdgesTheta[1]
                            )/pdfIntVal,
                       match = std::fabs((empiricPDF - theorPDF)/(empiricPDF + theorPDF));
            //std::cout << empiricPDF << " -- " << theorPDF << std::endl;  // XXX
            if( empiricPDF ) {
                normalized->SetBinContent( nBinX, nBinTheta, match );
            } else {
                normalized->SetBinContent( nBinX, nBinTheta, NAN );
            }
            sumEmpir += empiricPDF;
            sumTheor += theorPDF;
        }
    }

    return normalized;
}
# endif

} // namespace DPhMC

