/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "dphmc-config.h"

# include <assert.h>
# include <stdlib.h>
# include <stdio.h>
# include <stdint.h>
# include <math.h>
# include <unistd.h>
# include <time.h> 
# include <sys/types.h>
# include <sys/stat.h>
# include <string.h>
# include <strings.h>

# include <gsl/gsl_math.h>
# include <gsl/gsl_multimin.h>
# include <gsl/gsl_monte.h>
# include <gsl/gsl_monte_plain.h>
# include <gsl/gsl_monte_miser.h>
# include <gsl/gsl_monte_vegas.h>
# include <gsl/gsl_integration.h>
# include <gsl/gsl_rng.h>

# include "evGen/parameters.h"
# include "evGen/aprimeCrossSection.h"
# include "dphmc-logging.h"

# ifdef __cpluplus
extern "C" {
# endif

struct ParameterSet {
    # define declare_field( type, txtName, name, descr ) type name;
        for_all_PhMClib_aprimeCS_parameters( declare_field )
    # undef declare_field
    # define declare_field( type, txtName, dft, name, descr ) type name;
        for_all_PhMClib_aprimeCS_GSL_chi_comp_parameter( declare_field )
    # undef declare_field
};

/* Brief names to shorten the math */
# define UNPACK_PARAMETER_SET(PSName)       \
    const double A  = PSName->A,            \
                 Z  = PSName->Z,            \
                 AM = PSName->massA_GeV,    \
                 E0 = PSName->EBeam_GeV;    \
    /* avoid `unused' warnings: */          \
    ((void)(A)); ((void)(Z)); ((void)(AM)); ((void)(E0));

# define me     _DPhMC_CST_electronMass_GeV
# define alpha  _DPhMC_CST_fineStructureConstant
# define muP    _DPhMC_CST_muProton
# define mp     _DPhMC_CST_protonMass_GeV

/*
 * Workspace object
 * {{{ */

struct Workspace {
    struct ParameterSet parameterSet;
    double chi, tRange[2], chiRelError, chiAbsError;
    gsl_integration_workspace * gslIntWS;

    double xRange[2],
           thetaMax,
           integralEstVal ;  ///< according to (A16)
};

struct Workspace * /* blank workspace ctr */
_static_allocate_workspace() {
    struct Workspace * p = malloc( sizeof(struct Workspace) );
    bzero( p, sizeof(struct Workspace) );
    p->tRange[0] = p->tRange[1] = NAN;
    p->chi = NAN;
    p->gslIntWS = NULL;
    return p;
}

void /* workspace dtr */
_static_free_workspace( struct Workspace * w ) {
    if( w ) {
        if( w->gslIntWS ) {
            gsl_integration_workspace_free(w->gslIntWS);
        }
        free( w );
    }
}

static double
_static_calc_analytic_integral_estimation( void * wsPtr ) {
    struct Workspace * ws = (struct Workspace *) wsPtr;
    const struct ParameterSet * ps = &(ws->parameterSet);
    UNPACK_PARAMETER_SET(ps);
    //printf("\n\nm_{A'}=%eGeV\n\n", AM);
    /* See (A15) and footnote 2 on page 2 of arXiv 1209.6083 */
    /* Note: I tried to perform similar integration procedure and got factor 6
     * instead of 3 in denominater after dx integration, so predicted output
     * should be ~4 times lesser than (A15) predicts. */
    return /*(8./3)*/ /*(4./3)*/ (2./3)
          * pow((alpha*ps->epsilon)/AM, 2) * alpha * sqrt( 1 - pow(AM/E0, 2) )
          * ws->chi
          * log(1/pow(1 - ws->xRange[1], 2))
          * (ps->factor)
          /* _DPhMC_CST_pbarn_per_GeV * E0* E0*/;
}

/* }}} */

void
init_aprime_cross_section_workspace(
            # define declare_arg( type, txtName, name, descr ) type name,
                for_all_PhMClib_aprimeCS_parameters( declare_arg )
            # undef declare_arg
            # define declare_arg( type, txtName, dft, name, descr ) type name,
                for_all_PhMClib_aprimeCS_GSL_chi_comp_parameter( declare_arg )
            # undef declare_arg
            void ** wsPtr_ ) {

    /* Local enum, storing evaluation instructions */
    enum RoutineFlags {
            doRecalculation             = 0x1,
            doGSLWorkspaceAllocation    = 0x2 | 0x1,
            doParametersReassignment    = 0x4 | 0x1,
        } routineFlags = 0x0;

    struct Workspace ** wsPtr = (struct Workspace **) wsPtr_,
                      * ws = NULL;
    struct ParameterSet * ps = NULL;

    { /* decide, what to do: initialize workspace and parameter set pointers */
        struct ParameterSet newPs; {
            /* need to nullify alignment
               bytes for further comparison
             */ bzero( &newPs, sizeof(struct ParameterSet) );
            # define assign_field( type, txtName, name, descr ) newPs.name = name;
                for_all_PhMClib_aprimeCS_parameters( assign_field )
            # undef assign_field
            # define assign_field( type, txtName, dft, name, descr ) newPs.name = name;
                for_all_PhMClib_aprimeCS_GSL_chi_comp_parameter( assign_field )
            # undef assign_field
        }

        if( *wsPtr ) {
            /* if parameter set already allocated: */
            if( memcmp( &((*wsPtr)->parameterSet), &newPs, sizeof(struct ParameterSet) ) ) {
                /* if parameter set has changed: */
                if( (*wsPtr)->parameterSet.nnodes ) {
                    /* GSL nodes changed (probably, beside ither parameters ---
                     * we need to re-allocate GSL workspace) */
                    gsl_integration_workspace_free( (*wsPtr)->gslIntWS );
                    (*wsPtr)->gslIntWS = NULL;
                    routineFlags |= doGSLWorkspaceAllocation;
                }
                routineFlags |= doRecalculation;
                routineFlags |= doParametersReassignment;
            }
        } else {
            /* if parameter set wasn't allocated: */
            *wsPtr = _static_allocate_workspace();
            routineFlags |= doRecalculation;
            routineFlags |= doParametersReassignment;
        }
        ws = *wsPtr;
        ps = &(ws->parameterSet);
        if( routineFlags & doParametersReassignment ) {
            memcpy( &((*wsPtr)->parameterSet), &newPs, sizeof(struct ParameterSet) );
        }
    }

    if( routineFlags & doGSLWorkspaceAllocation ) {
        ws->gslIntWS = gsl_integration_workspace_alloc( ps->nnodes );
    }

    if( routineFlags & doRecalculation ) {
        /*DPhMC_msg( 3, "Recalculation of chi form-factor invoked for E0=%e.\n", ps->EBeam_GeV );*/
        ws->tRange[0] = pow(ps->massA_GeV, 4)/(4*(ps->EBeam_GeV)*(ps->EBeam_GeV));
        ws->tRange[1] = ps->massA_GeV*ps->massA_GeV;
        ws->chi = chi( &(ws->chiAbsError), &(ws->chiRelError), ws );
        if(!(isfinite(ws->chi) &&
             isfinite(ws->tRange[0]) &&
             isfinite(ws->tRange[1])) ) {
            DPhMC_error( DPhMC_E_badValue,
                         "Bad value of t_min=%e/t_max=%e/chi=%e.",
                                ws->tRange[0],
                                ws->tRange[1],
                                ws->chi );
        }

        /* from common sense */
        ws->xRange[0] = massA_GeV / EBeam_GeV;
        {   /* from condition (A16) */
            double c1 = _DPhMC_CST_electronMass_GeV/massA_GeV,
                   c2 = ws->xRange[0];
            if( c1 > c2 ) {
                ws->xRange[1] = 1 - c1;
            } else {
                ws->xRange[1] = 1 - c2;
            }
        }
        if( ws->xRange[0] > 1 || ws->xRange[0] < 0. ) {
            DPhMC_msgw( "x_min > 1 || x_min < 0 for the A' integration workspace %p"
                         "(E_0 = %e GeV, x_{min} = %e)!\n",
                         ws, EBeam_GeV, ws->xRange[0] );
        }
        if( ws->xRange[1] > 1 || ws->xRange[1] < 0. ) {
            DPhMC_msgw( "x_max > 1 || x_max < 0 for the A' integration workspace %p"
                         "(E_0 = %e GeV, x_{max} = %e)!\n",
                         ws, EBeam_GeV, ws->xRange[1] );
        }
        if( ! (ws->xRange[0] < ws->xRange[1]) ) {
            DPhMC_msge( "x_min >= x_max for the A' integration workspace %p"
                         "(E_0 = %e GeV, x_{min} = %e, x_{max} = %e)!\n",
                         ws, EBeam_GeV, ws->xRange[0], ws->xRange[1] );
        }
        /* according to formual (9) */
        {
            double val1 = sqrt( massA_GeV*_DPhMC_CST_electronMass_GeV )/EBeam_GeV,
                   val2 = pow( massA_GeV/EBeam_GeV, 1.5 );
            ws->thetaMax = (val1 > val2 ? val1 : val2);
            # if 0
            /* Prints out theta median value calculation. */
            printf( ">>>>>>>>>>>>>>>>>>>>>>\n"
                    "      m_e = %e GeV\n"
                    "      E_0 = %e GeV\n"
                    "   m_{A'} = %e GeV\n"
                    " \\theta_m = %e\n"
                    ,
                    _DPhMC_CST_electronMass_GeV,
                    EBeam_GeV,
                    massA_GeV,
                    ws->thetaMax );
            # endif
            # if 1
            /* Set theta upper cut-off value up to 300 times greater than
             * median value. TODO: parameterise it somewhere? */
            ws->thetaMax *= 1e3;
            if( ws->thetaMax > M_PI ) {
                /* Almost impossible, however, just in case */
                DPhMC_msgw( "Upper bound for thetaMax exceeds Pi. Set it to Pi.\n" );
                ws->thetaMax = M_PI;
            }
            # else
            /* according to arXiv 1209.6083, comment above formula (5). */
            ws->thetaMax = .5;
            # endif
        }
        ws->integralEstVal = _static_calc_analytic_integral_estimation( ws );
    }
}

void
free_aprime_cross_section_workspace( void * ws_ ) {
    if( !ws_ ) return;
    _static_free_workspace( (struct Workspace *) ws_ );
}

/** For given nuclei parameters, calculates integrand function as a summation
 * of elastic/inelastic components.
 * see \ref{JDBjorken} (A18,19). */
static double
chi_integrand(double t, void * ws_) {
    struct Workspace * ws = (struct Workspace *) ws_;
    const struct ParameterSet * ps = &(ws->parameterSet);
    UNPACK_PARAMETER_SET(ps);

    const double d = 0.164/pow(A, 2./3),
                ap = 773/(pow(Z, 2./3)*me),
                 a = 111/(pow(Z, 1./3)*me),
                a2 = a*a,
                t2 = t*t,
               ap2 = ap*ap,
              G2el = pow( a2*t*Z/( (1 + a2*t)*(1 + t/d) )
                  , 2),
              G2in = Z*pow( ( (ap2*t)/(1+ap2*t) )*( 1+t*(muP*muP-1)/(4*mp*mp) ) /
                                    pow(1 + t/.71, 4)
                  , 2 ),
                G2 = G2el + G2in,
               res = G2*(t - ws->tRange[0])/t2
             ;

    if( !isfinite(res) ) {
        DPhMC_error( DPhMC_E_badValue,
                     "chi_integrand() evaluated to bad value: %e.", res);
    }

    # if 0
    {
        double ratio = t/(4*mp*mp);
        if( ratio >= 1.e3 ) {
            goo_c_warn("chi() -- condition of t/(4*mp*mp(=%e)) < 1e3 failed!\n", ratio );
        }
    }
    # endif

    return res;
}


double
chi( double * absError,
     double * relError,
     void * ws_ ) {
    assert(ws_);
    struct Workspace * ws = (struct Workspace *) ws_;
    const struct ParameterSet * ps = &(ws->parameterSet);
    UNPACK_PARAMETER_SET(ps);
    if( ps->epsrel <= 0. ) {
        DPhMC_msg( -1, "refusing evaluate chi() routine due to zero relativeerror tolreance specified.\n" );
        return NAN;
    }

    double chi_ = NAN; {
        gsl_function F = {
                .function = &chi_integrand,
                .params = ws_
            };
        int rr;

        gsl_error_handler_t * old_handler=gsl_set_error_handler_off();
        for( *relError = ps->epsrel;
             *relError < 1.;
             *relError *= ps->epsrelIncFt ) {
            if( GSL_EROUND == (rr = gsl_integration_qags(
                    &F,
                    ws->tRange[0],
                    ws->tRange[1],
                    ps->epsabs,
                    *relError,
                    ps->limit,
                    ws->gslIntWS,
                    &chi_,
                    absError)) ) {
                DPhMC_msg( -1, "chi parameter integration: at gsl_integration_qags(...) needs\
to be more tolerant [%e, %e]; changing epsrel %e -> %e.\n",
                                ws->tRange[0],
                                ws->tRange[1],
                                *relError,
                                (*relError) * ps->epsrelIncFt);
                continue;
            } else if( !rr ) {
                break;  /* integrated correctly */
            } else {
                # if 1
                    DPhMC_msg( 3, "A' CS integration workspace:\n" );
                    # define print_parameter(type, txtName, name, descr) \
                            DPhMC_msg( 3, "  %s ... %e\n", txtName, (double) ps-> name );
                        for_all_PhMClib_aprimeCS_parameters(print_parameter)
                    # undef print_parameter
                    # define print_parameter(type, txtName, dft, name, descr) \
                            DPhMC_msg( 3, "  %s ... %e\n", txtName, (double) ps-> name );
                        for_all_PhMClib_aprimeCS_GSL_chi_comp_parameter(print_parameter)
                    # undef print_parameter
                # endif
                DPhMC_msg( 3, "  low integration limit ... %e\n", ws->tRange[0] );
                DPhMC_msg( 3, "  high integration limit ... %e\n", ws->tRange[1] );
                DPhMC_msg( 3, "  current rel error ... %e\n", *relError );
                DPhMC_msge( "Got a gsl_integration_qags(...) error %d: \"%s\".\n",
                             rr,
                             (gsl_strerror(rr) ? gsl_strerror(rr) : "<unknown>" ) );
                DPhMC_error( DPhMC_E_thirdParty,
                             "Got a gsl_integration_qags(...) error %d: \"%s\"",
                             rr,
                             (gsl_strerror(rr) ? gsl_strerror(rr) : "<unknown>" ));
            }
        }
        gsl_set_error_handler(old_handler);
    }
    if( !isfinite(chi_) ) {
        DPhMC_error( DPhMC_E_badValue,
                     "chi() integration evaluated to bad value: %e.",
                     chi_);
    }

    return chi_;
}

double
aprime_cross_section_a12( double x,
                      double theta,
                      void * ws_ ) {
    assert(ws_);
    struct Workspace * ws = (struct Workspace *) ws_;
    const struct ParameterSet * ps = &(ws->parameterSet);
    UNPACK_PARAMETER_SET(ps);

    /*goo_c_log3( "Sigma calculation on E0=%e.\n", E0 );*/

    const double
        betaAPrime = sqrt( 1 - pow(AM/E0, 2) ),
        am2 = AM*AM,
        U = pow(E0*theta, 2)*x + am2*(1 - x)/x + me*me*x,
        U2 = U*U,
        factor1 = (1 - x + x*x/2)/U2,
        factor2 = pow( (1 - x)*AM/U2, 2),
        factor3 = am2 - (U*x)/(1 - x),
        factor4 = 2*alpha*pow(2*alpha*ps->epsilon, 2)*betaAPrime*(ws->chi),
        res = factor4*(factor1 + factor2*factor3)/*(E0*E0*x)*/ /*_DPhMC_CST_pbarn_per_GeV*/;
    if( !isfinite(res) ) {
        DPhMC_msg( -1, "           x ... %e\n", x );
        DPhMC_msg( -1, "       theta ... %e\n", theta );
        DPhMC_msg( -1, "         A'm ... %e\n", AM );
        DPhMC_msg( -1, "          E0 ... %e\n", E0 );
        DPhMC_msg( -1, "  betaAPrime ... %e\n", betaAPrime );
        DPhMC_msg( -1, "           U ... %e\n", U );
        DPhMC_msg( -1, "     factor1 ... %e\n", factor1 );
        DPhMC_msg( -1, "     factor2 ... %e\n", factor2 );
        DPhMC_msg( -1, "     factor3 ... %e\n", factor3 );
        DPhMC_msg( -1, "     factor4 ... %e\n", factor4 );
        DPhMC_error( DPhMC_E_badValue,
                     "aprime_cross_section_a12() routine evaluated to "
                     "bad value: %e.", res );
    }

    return res * (ps->factor);
}

double
lower_cut_x( void * wsPtr ) {
    struct Workspace * ws = (struct Workspace *) wsPtr;
    return ws->xRange[0];
}

double
upper_cut_x( void * wsPtr ) {
    struct Workspace * ws = (struct Workspace *) wsPtr;
    return ws->xRange[1];
}


double
upper_cut_theta( void * wsPtr ) {
    struct Workspace * ws = (struct Workspace *) wsPtr;
    return ws->thetaMax;
}

double
analytic_integral_estimation( void * wsPtr ) {
    struct Workspace * ws = (struct Workspace *) wsPtr;
    return ws->integralEstVal;
}

# if 0
double
aprime_cs_at_border( void * ws_ ) {
    assert(ws_);
    struct Workspace * ws = (struct Workspace *) ws_;
    const struct ParameterSet * ps = &(ws->parameterSet);
    UNPACK_PARAMETER_SET(ps);

    const double
        betaAPrime = sqrt( 1 - pow(AM/E0, 2) );

    return alpha*pow(2*alpha*ps->epsilon, 2)*ws->chi*betaAPrime*E0*E0/pow(me, 4)*_DPhMC_CST_pbarn_per_GeV;
}
# endif

# ifdef __cpluplus
}  // extern "C"
# endif

