/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "dphmc-logging.h"

# include <stdarg.h>

# define assign_errcode_const( code, name, description )   \
    const DPhMC_ErrorCode DPhMC_E_ ## name = code;
for_all_DPhMC_error_codes( assign_errcode_const )
# undef assign_errcode_const

FILE * G_DPhMC_msgstream = NULL;
FILE * G_DPhMC_errstream = NULL;
signed char G_DPhMC_verbosity = 1;

void
DPhMC_msg( int level, const char * fmtstr, ... ) {
    FILE * _G_DPhMC_msgstream = G_DPhMC_msgstream,
         * _G_DPhMC_errstream = G_DPhMC_errstream
         ;

    if( !_G_DPhMC_msgstream ) {
        _G_DPhMC_msgstream = stdout;
    }
    if( !_G_DPhMC_errstream ) {
        _G_DPhMC_errstream = stderr;
    }

    char strBuf[256];
    va_list args;
    va_start( args, fmtstr );
    vsnprintf( strBuf, sizeof(strBuf),
              fmtstr, args );
    va_end( args );
    if( !level || level <= G_DPhMC_verbosity ) {
        fputs( strBuf, _G_DPhMC_msgstream );
    } else if( level < 0 ) {
        fputs( strBuf, _G_DPhMC_errstream );
    }
}



