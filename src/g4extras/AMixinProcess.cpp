/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "g4extras/AMixinProcess.hpp"

# include <TRandom.h>
# include <TLorentzVector.h>

# include "g4extras/APrimeProduction.hpp"
# include "evGen/aprimeEvGen.hpp"
# include "evGen/aprimeCSCache.hpp"

namespace DPhMC {

# ifndef AMIXING_PROCESS_USES_HADRONIC_API
AMixingProcess::AMixingProcess( TRandom * rnd,
                                double AMass,
                                double mixingConstant ) :
            G4VDiscreteProcess( DPhMC_APRIME_MIXING_PROCESS_NAME, fUserDefined ),
            theAPrimePtr( DPhMC::APrime::Definition() ),
            _AMass( AMass ),
            _mixingConstant( mixingConstant ) {
    DPhMC::APrimeCSCaches::self().random_generator( rnd );
    SetProcessSubType( 83 );  // TODO: correct this
}

AMixingProcess::~AMixingProcess() {
}

struct CompareGeneratorCachingPair {
    bool operator()( const std::pair<double, DPhMC::APrimeGenerator *> & l,
                     const std::pair<double, DPhMC::APrimeGenerator *> & r ) {
        return l.first < r.first;
    }
    bool operator()( const std::pair<double, DPhMC::APrimeGenerator *> & l,
                     const double & rv ) {
        return l.first < rv;
    }
};

G4VParticleChange *
AMixingProcess::PostStepDoIt( const G4Track & aTrack,
			                  const G4Step & /*aStep*/ ) {
    //DPhMC_msg1( " >>> >>> AMixingProcess::PostStepDoIt() invoked.\n" );
    // Get interacting particle.
    //const G4DynamicParticle & projectile = *(aTrack.GetDynamicParticle());

    // Choose an element in material to be used as the target.
    decltype(_mruGeneratorsCache)::iterator choosenGeneratorIt = _mruGeneratorsCache.begin();
    assert(!_mruGeneratorsCache.empty());
    if( 1 != _mruGeneratorsCache.size() ) {
        // For heterogeneous medium, choose randomly an element between presented in media:
        double randVal = const_cast<TRandom *>(
            DPhMC::APrimeCSCaches::self().random_generator())->Uniform(0., _mruMFP);
        choosenGeneratorIt = std::lower_bound<
                                        decltype(_mruGeneratorsCache)::iterator
                                        , double
                                        , CompareGeneratorCachingPair>
                        ( _mruGeneratorsCache.begin(), _mruGeneratorsCache.end(),
                          randVal, CompareGeneratorCachingPair() );
        if( choosenGeneratorIt == _mruGeneratorsCache.end() ) {
            // This error may indicate wrong sequence of GetMeanFreePath()/PostStepDoIt()
            // invokations. We came from an assumption that PostStepDoIt() are called
            // strictly AFTER GetMeanFreePath() in one thread.
            DPhMC_exception_throw( badState, "Failed to find out a lower bound among cached values." );
        }
    } // otherwise use this single generator:
    DPhMC::APrimeGenerator & g = *(choosenGeneratorIt->second);

    // Generate A'
    DPhMC::APrimeGenerator::Event e;
    g.generate( e );

    // Now we have A' theta angle and energy in Lab frame.

    const double
            // E0, energy of initial projectile:
            E_proj = aTrack.GetKineticEnergy(),
            // Azimutal angle of A':
            phi_Ap = const_cast<TRandom *>(
                        DPhMC::APrimeCSCaches::self().random_generator())->Uniform(-M_PI, M_PI),
            // Energy of A' in GeVs:
            E_Ap = e.byName.x*E_proj,
            // Recoil particle energy (former projectile, C3 \cite{Bjorken}):
            E_recoil = (1 - e.byName.x)*E_proj,
            // Recoil particle polar angle (former projectile, C4 \cite{Bjorken}):
            theta_recoil = sqrt( ::DPhMC::APrimeCSCaches::APrimeMass / E_proj ) * 
                    (1 + ::DPhMC::APrimeCSCaches::APrimeMass / E_proj /* + ... todo: series? */ )
        ;

    // Initialize A' direction vector:
    G4ThreeVector aprimeDirection(0., 0., 1); {
        aprimeDirection.setMag(1.);
        aprimeDirection.setTheta( e.byName.theta );
        aprimeDirection.setPhi( phi_Ap );
    }
    // Initialize projectile particle direction vector:
    G4ThreeVector projDirection(0., 0., 1); {
        projDirection.setMag(1.);
        projDirection.setTheta( theta_recoil );
        projDirection.setPhi( -phi_Ap );
    }

    G4DynamicParticle * movingAPrime = 
        new G4DynamicParticle( theAPrimePtr,
                               aprimeDirection,
                               E_Ap );

    aParticleChange.Initialize( aTrack );

    // Set A':
    aParticleChange.SetNumberOfSecondaries( 1 );
    aParticleChange.AddSecondary( movingAPrime );

    // Set projectile changes:
    # if 0
    // Note: if "MomentumDirection" means, actually, momentum, one
    // need to calculate momentum precisely. Possibly, using this
    // method:
    G4ThreeVector p_e = aParticleChange.CalcMomentum( E_recoil,  );
    # else
    aParticleChange.ProposeEnergy( E_recoil );
    aParticleChange.ProposeMomentumDirection( projDirection );
    # endif
    DPhMC_msg1( "AMixingProcess::PostStepDoIt() created secondary.\n" );
    // 
    # if 0
    TLorentzVector projectileV_lab,
                   aprimeV_lab; {
        TLorentzVector projectileV_cm, aprimeV_cm;
        TVector3 aprimeV3_lab;
        const double E0 = aTrack.GetKineticEnergy(),
                     V = sqrt( 1 - E0*E0 )
                     ;

        // Set a momentum of A':
        {
            const double EAprime = E0*e.byName.x,
                         gammaAPrime = DPhMC::APrimeCSCaches::APrimeMass/(EAprime),
                         betaAPrime = sqrt( 1 - gammaAPrime*gammaAPrime ),
                         pAPrime_lab = betaAPrime*EAprime
                         ;
            aprimeV3_lab.SetMag( pAPrime_lab );  // TODO: p = \beta E = \beta x E_0
        }

        aprimeV3_lab.SetTheta( e.byName.theta );
        aprimeV3_lab.SetPhi( const_cast<TRandom *>(
            DPhMC::APrimeCSCaches::self().random_generator())->Uniform(-M_PI, M_PI) );
        

        // Set a momentum of projectile as:
        projectileV_cm = - aprimeV_cm;
        projectileV_lab = projectileV_cm; projectileV_lab.Boost( 0., 0., -V);
    }
    # endif
    return &aParticleChange;
}

/** Calculates mean free path based on material compound and particle type,
 * routing the actual generators that should be used for particular
 * conditions.
 *
 * TODO: currently, only heavy materials for only electron projectile is
 * supported for A' production. «Heavy» means that its A > matMinA value
 * set by 
 */
G4double
AMixingProcess::GetMeanFreePath( const G4Track & aTrack,
                                 G4double /*previousStepSize*/,
                                 G4ForceCondition * condition ) {
    _mruGeneratorsCache.resize(0);
    _mruMFP = 0.;
    assert( aTrack.GetDefinition()->GetPDGCharge() );
    const double incidentE = aTrack.GetKineticEnergy()/::CLHEP::GeV;

    if( incidentE < minimal_projectile_energy() ) {
        // Has no suitable generator for this energy.
        return DBL_MAX;
    }

    double & lambdaInv = _mruMFP;
    size_t nEl = 0;

    const G4ElementVector & elList   = *( aTrack.GetMaterial()->GetElementVector() );
    *condition = NotForced;
    //const G4double * fractions = *( aTrack.GetFractionVector() ); // XXX?
    // Following code implements formula for interaction length on page 8 of
    // http://geant4.web.cern.ch/geant4/UserDocumentation/UsersGuides/PhysicsReferenceManual/fo/PhysicsReferenceManual.pdf
    for( auto it = elList.cbegin(); elList.cend() != it; ++it, ++nEl ) {
        G4Element & el = **it;
        if( el.GetAtomicMassAmu() < _minAWW ) continue;  // omit light materials
        try {
            DPhMC::APrimeGenerator * g = DPhMC::APrimeCSCaches::self().generator(
                                aTrack.GetDefinition()->GetPDGCharge(),
                                aTrack.GetDefinition()->GetPDGMass(),
                                incidentE,
                                el.GetAtomicMassAmu(), el.GetZ() );
            // TODO: units check
            // Note: probability() without arguments returns, actually, an integrated
            // cross section in GeVs
            const double integralCS_GeV = g->integrand().probability();
            // Some obvious checks:
            assert( ::std::isfinite(integralCS_GeV) );
            assert( integralCS_GeV );
            assert( integralCS_GeV > 0 );
            // Increase mean free path for this energy now:
            double nCDotSigma = aTrack.GetMaterial()->GetVecNbOfAtomsPerVolume()[nEl]
                                * DPhMC::APrimeWWPDF::convert_cross_section_units_to_CLHEP_units(integralCS_GeV);
            _mruGeneratorsCache.push_back( { lambdaInv +=  nCDotSigma, g } );
        } catch( DPhMC::Exception & e ) {
            DPhMC_msge( "Error occured during A' CS acquizition: %s\n", e.what() );
            // todo: dump stacktrace to err-log stream
            return DBL_MAX;
        }
    }

    if( _mruGeneratorsCache.empty() ) {
        // No elements have sufficient mass.
        return DBL_MAX;
    }

    if( lambdaInv > 0. ) {
        return 1/lambdaInv;
    } else {
        DPhMC_msge( "Erroneous \\lambda=%e (<=0).\n", lambdaInv );
        return DBL_MAX;
    }
}

G4bool
AMixingProcess::IsApplicable(const G4ParticleDefinition & pDef) {
    if( !pDef.GetPDGCharge() ) {
        return false;
    }
    return true;
    // ... whatever?
}

# else  // AMIXING_PROCESS_USES_HADRONIC_API
///////////////////////////////////////////////////////////////////////////////
AMixingProcess::AMixingProcess() :
    G4HadronicProcess( "A'-mixing", fUserDefined ) {
    _mruGeneratorsCache.reserve(50);
    // Note: fUserDefined is of enum type `G4ProcessType'. It is more suitable
    // to use enumeration type `G4HadronicProcessType', but there is no user
    // declaration.
    // TODO: check cleanup
    this->GetManagerPointer()->RegisterMe(new APrimeProduction());
    // TODO: APrimeCrossSectionsCache lazy initialization
}

AMixingProcess::~AMixingProcess() {
}


G4VParticleChange *
AMixingProcess::PostStepDoIt( const G4Track& track,
                              const G4Step& step) {
    _TODO_  // TODO
}


G4double
AMixingProcess::GetMeanFreePath( const G4Track &    aTrack,
                                 G4double           previousStepSize,
                                 G4ForceCondition * condition ) {
    return 0;  // TODO
}
# endif  // AMIXING_PROCESS_USES_HADRONIC_API

}  // namespace DPhMC

