/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "dphmc-config.h"

# include "dphmc-utils.hpp"
# include "g4extras/AParticle.hpp"
# include "g4extras/AMixinProcess.hpp"
# include "g4extras/APhysics.hpp"
# include "evGen/aprimeCSCache.hpp"

# ifdef DPhMC_STROMAV_INTEGRATION
#   include "g4extras/PhysList.hpp"  // (from StromaV)
# endif

# include <TRandom3.h>  // XXX (see TODO)

# include <G4VProcess.hh>
# include <G4ProcessVector.hh>
# include <G4ProcessManager.hh>
# include <G4HadronicProcess.hh>
# include <G4Version.hh>
# include <G4StepLimiter.hh>

# if G4VERSION_NUMBER > 999
# if defined( __clang__ ) && ! defined( G4MULTITHREADED )
// When using aParticleIterator without multithreading support, an in-template
// static field is used for .offset attr. This causes Clang to complain about
// actual attribute instantiation. The problem is, besides of this reasonable
// notion, Clang may generate multiple instances for such attributes when
// they're operating in different threads. To suppress this annoying warning we
// define it here in hope it won't lead to dangerous consequencies.
template<> G4VPCData * G4VUPLSplitter<G4VPCData>::offset;
# endif  // G4_TLS
# endif

namespace DPhMC {

G4VPhysicsConstructor *
construct_aprime_physics( TRandom * rndg ) {
    DPhMC::APrimeCSCaches::APrimeMass =
            Config::instance().option_as<double>("massA_GeV");
    std::set<double> energies = DPhMC::parse_aprime_tabulation_series(
                    Config::instance().option_as<std::string>("energyTabulationFactors"),
                    DPhMC::APrimeCSCaches::APrimeMass,
                    Config::instance().option_as<double>("EBeam_GeV") );
    DPhMC::APrimeCSCaches::MixingConstant =
                Config::instance().option_as<double>("mixingFactor");
    auto tr3Seed = Config::instance().option_as<uint32_t>("trand3Seed");
    if( !rndg ) {
        rndg = new TRandom3(tr3Seed);
    }
    DPhMC::APrimeCSCaches::CSMultFactor = 
                Config::instance().option_as<double>("factor");
    return new ::DPhMC::APrimePhysics(
                /* Verbosity ... */ 3,
                DPhMC::APrimeCSCaches::APrimeMass,
                DPhMC::APrimeCSCaches::MixingConstant,
                rndg,
                energies,
                Config::instance().option_as<int>("matMinAWWApprox"), // matMinAWWApprox
                Config::instance().option_as<bool>("considerDecay") // enableAPrimeDecay
            );
}

}  // namespace DPhMC

# ifdef DPhMC_STROMAV_INTEGRATION
namespace sV {
template<> ::DPhMC::APrimePhysics *
ModularPhysicsList::construct_physics<::DPhMC::APrimePhysics>() {
    return static_cast<::DPhMC::APrimePhysics *>(DPhMC::construct_aprime_physics());
}
}  // namespace sV
# endif  // DPhMC_STROMAV_INTEGRATION

namespace DPhMC {

G4VProcess * APrimePhysics::_mixingProcessInstancePtr = nullptr;

/**@brief Default ctr to physics module.
 *
 * Initializes tabulation range for caching container.
 * 
 * @param verbosity level of physics module (0-3).
 * @param aprimeMass assumed mass of A' particle (TODO: why it is here?).
 * @param aprimeMixing assumed mixing parameter within WW approximation.
 * @param rndGenerator pointer to random number generator.
 * @param energies a set of energies for which particular generators will be created.
 * @param matMinAWWApprox minimal atomic number for generator to be produced.
 * @param enableAPrimeDecay enables A' decay physics.
 */
APrimePhysics::APrimePhysics( int verbosity,
                              double aprimeMass,
                              double aprimeMixing,
                              TRandom * rndGenerator,
                              const std::set<double> & energies,
                              uint8_t matMinAWWApprox,
                              bool enableAPrimeDecay ) :
        G4VPhysicsConstructor( "APrimePhysics" ),
        _verbosityLevel( verbosity ),
        _aPrimeMass( aprimeMass ),
        _mixingConstant( aprimeMixing ),
        _rndGen( rndGenerator ),
        _matMinAWWApprox( matMinAWWApprox ),
        _decayEnabled( enableAPrimeDecay ) {
    DPhMC::APrimeCSCaches::self().projectile_energy_tabulation(energies);
}

APrimePhysics::~APrimePhysics() {
    (void)(_verbosityLevel);  // XXX: suppress `unused' warning,
                              // keep field for further usage.
}

void
APrimePhysics::ConstructParticle() {
    APrime::Definition();
    G4Electron::Electron();
    //G4Positron::Positron();
    //G4MuonPlus::MuonPlus();
    //G4MuonMinus::MuonMinus();
    //G4Proton::Proton();
}

void
APrimePhysics::ConstructProcess() {
    //G4PhysicsListHelper* ph = G4PhysicsListHelper::GetPhysicsListHelper();
    # if G4VERSION_NUMBER > 999
    // This line causes clang to doubt about unavailable definition. To supress
    // his warnings, we use another form of the same thing:
    # if 0
    for( theParticleIterator->reset(); (*aParticleIterator)() ; )
    # else
    auto localParticleIterator =
            (G4VPhysicsConstructor::GetSubInstanceManager()
                        .offset[g4vpcInstanceID])._aParticleIterator;
    for( localParticleIterator->reset();
         (*localParticleIterator)() ; )
    # endif
    # else
    for( theParticleIterator->reset(); (*theParticleIterator)() ; )
    # endif
    {
        G4ParticleDefinition & particle =
        # if G4VERSION_NUMBER > 999
        # if 0
        aParticleIterator->value();
        # else
        *(localParticleIterator->value());
        # endif
        # else  // > 999
        *(theParticleIterator->value());
        # endif  // > 999

        if( !particle.GetPDGCharge() ) { continue; }  // bypass uncharged particles

        // todo: currently, only "e-" is supported:
        if( "e-" == particle.GetParticleName()
         || "e+" == particle.GetParticleName()) {
            if( !_mixingProcessInstancePtr ) {
                auto proc = new AMixingProcess( _rndGen, a_prime_mass(), mixing_constant() );
                _mixingProcessInstancePtr = proc;
                DPhMC_msg1( "E/M A' mixing process constructed: "
                    "%p.\n", _mixingProcessInstancePtr );
                proc->minimal_material_A( _matMinAWWApprox );
                proc->minimal_projectile_energy(
                        *DPhMC::APrimeCSCaches::self().projectile_energy_tabulation().begin() );
                DPhMC_msg3( "Minimal considered energy for A' production is set to %e GeV.\n",
                                proc->minimal_projectile_energy() );
                DPhMC_msg3( "Minimal considered atomic number for A' production is set to %d.\n",
                                (int) proc->minimal_material_A() );
            }
            G4ProcessManager & pmanager = *particle.GetProcessManager();
            pmanager.AddDiscreteProcess( _mixingProcessInstancePtr );
            //std::cout << "XXX Process type ...... : " << proc->GetProcessType() << std::endl
            //          << "XXX PRocess subtype ... : " << proc->GetProcessSubType() << std::endl
            //          ;
            //ph->SetVerboseLevel(3); // unsupported usually
            //ph->RegisterProcess( proc, &particle );
            //todo? Do we need this:
            //ph->RegisterProcess(new G4StepLimiter,  &particle );
        }

        # if 0
        //auto * processPtr = new G4ElectronNuclearProcess(); ( TODO: obtain already associated processes )
        G4ProcessManager & procManager = *particle.GetProcessManager();
        G4ProcessVector & procVector = *procManager.GetProcessList();
        _ss << particle.GetParticleName() << ":" << std::endl;
        for( int nProcess = 0; nProcess < procVector.size(); ++nProcess ) {
            G4VProcess & process = *procVector[nProcess];
            _ss << "   ... " << process.GetProcessName();
            G4HadronicProcess * hProcPtr = dynamic_cast<G4HadronicProcess *>( &process );
            if( hProcPtr ) {
                _ss << " (" ESC_CLRBOLD "hadronic process descendant" ESC_CLRCLEAR ")";
                //hProcPtr->RegisterMe( new AnyChargedAprimeMixinModel() );  // TODO !!!
                //procManager.AddDiscreteProcess( hProcPtr ); // bad approach
            }
            _ss << std::endl;
        }
        //processPtr->RegisterMe( modelPtr );
        //ph->RegisterProcess( processPtr, &particle );
        // TODO: obtain
        # endif

        # if 0
        G4String particleName = particle->GetParticleName();
        G4VDiscreteProcess * fSRInMat = ( _inMaterials ?
                dynamic_cast<G4VDiscreteProcess *>(new G4SynchrotronRadiationInMat()) :
                dynamic_cast<G4VDiscreteProcess *>(new G4SynchrotronRadiation()) );
        # if G4VERSION_NUMBER > 999
        G4AutoDelete::Register(fSR);  // introduced since Geant 4.10
        # endif
        if( particleName=="e-" ) {
            ph->RegisterProcess(fSRInMat,           particle );
            ph->RegisterProcess(new G4StepLimiter,  particle );
        } else if( particleName=="e+" || particleName=="mu+"
                                      || particleName=="mu-"
                                      || particleName=="proton" ) {
            // XXX Previous way: procMan->AddProcess(fSR,               -1, -1, 5 );
            ph->RegisterProcess(fSRInMat,           particle );
            ph->RegisterProcess(new G4StepLimiter,  particle );
        } else if (particle->GetPDGCharge() != 0.0 &&
                 !particle->IsShortLived()) {
            ph->RegisterProcess(fSRInMat,           particle );
        }
        # endif
    }
    // TODO
    if( is_decay_enabled() ) {
        _TODO_  // TODO
    }
    DPhMC_msg2( "Constructed processes in A' physics module.\n" );  // todo: details?
}

# ifdef DPhMC_STROMAV_INTEGRATION
REGISTER_PHYSICS_MODULE( APrimePhysics );  // TODO: StromaV register dyn. module
# endif  // DPhMC_STROMAV_INTEGRATION

}  // namespace DPhMC

