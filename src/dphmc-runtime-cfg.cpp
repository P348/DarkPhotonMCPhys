/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "dphmc-runtime-cfg.hpp"
# include "evGen/parameters.h"

# ifdef DPhMC_STROMAV_INTEGRATION
#   include "app/abstract.hpp"  // (from StromaV)
# endif

namespace DPhMC {

Config * Config::_self = nullptr;

Config::Config() {
    # define ins_par_wt_defval( type, strName, name, descr ) \
        this->emplace( strName, Entry(boost::any(), descr) );
    for_all_PhMClib_aprimeCS_parameters( ins_par_wt_defval )
    # undef ins_par_wt_defval
    set<double>("factor", 1);
    # define ins_par_w_defval( type, strName, defaultVal, name, descr ) \
        this->emplace( strName, Entry(boost::any((type) defaultVal), descr) );
    for_all_PhMClib_aprimeCS_GSL_chi_comp_parameter( ins_par_w_defval )
    for_all_PhMClib_TFoam_generator_parameters( ins_par_w_defval )
    # undef ins_par_w_defval
    this->emplace( "energyTabulationFactors",
            Entry( std::string(".1 .11 .12 .13 .14 .15 .17 .2 .25 .4 .6 .8 1."),
                   "Series of factors to be applied to maximum" ) );
    this->emplace( "matMinAWWApprox",
            Entry( 60,
                   "Minimal atomic number to be considered." ) );
    this->emplace( "considerDecay",
            Entry( false,
                   "Whether to model the visible decay process of A'." ) );

    # ifndef DPhMC_STROMAV_INTEGRATION
    this->emplace( "energyTabulationFactors",
            Entry( std::string(".1 .11 .12 .13 .14 .15 .17 .2 .25 .4 .6 .8 1."),
                   "Series of factors to be applied to maximum" ) );
    this->emplace( "matMinAWWApprox",
            Entry( 60,
                   "Minimal atomic number to be considered." ) );
    this->emplace( "considerDecay",
            Entry( false,
                   "Whether to model the visible decay process of A'." ) );
    # else

    // When StromaV enabled, the run-time configuration parameters will be
    // pre-set from its current app config:
    //
    auto & app = goo::app<sV::AbstractApplication>();
    //
    # define copy_sV_parameter( type, strName, name, description ) \
        set<type>( strName, app.cfg_option<type>("aprimeEvGen." strName) );
    for_all_PhMClib_aprimeCS_parameters( copy_sV_parameter )
    # undef copy_sV_parameter
    //
    # define copy_sV_parameter( type, strName, defVal, name, description ) \
        set<type>( strName, app.cfg_option<type>("extraPhysics.physicsAe.gslIntegration." strName) );
    for_all_PhMClib_aprimeCS_GSL_chi_comp_parameter( copy_sV_parameter )
    # undef copy_sV_parameter
    //
    # define copy_sV_parameter( type, strName, defVal, name, description ) \
        set<type>( strName, app.cfg_option<type>("extraPhysics.physicsAe.TFoam." strName) );
    for_all_PhMClib_TFoam_generator_parameters( copy_sV_parameter )
    # undef copy_sV_parameter
    # endif
}

boost::any
Config::option( const std::string & key ) const {
    auto it = this->find( key );
    if( this->end() == it ) {
        DPhMC_exception_throw( notFound,
                               "Run-time config has no parameter \"%s\".",
                               key.c_str() );
    }
    if( it->second.first.empty() ) {
        DPhMC_exception_throw( badParameter,
                               "Parameter \"%s\" of run-time config is not set.",
                               key.c_str() );
    }
    return it->second.first;
}

boost::any &
Config::mutable_option( const std::string & key ) {
    auto it = this->find( key );
    if( this->end() == it ) {
        DPhMC_exception_throw( notFound,
                               "(mutable) Run-time config has no parameter \"%s\".",
                               key.c_str() );
    }
    return it->second.first;
}

Config &
Config::instance() {
    if( !_self ) {
        _self = new Config();
    }
    return *_self;
}

}  // namespace DPhMC

