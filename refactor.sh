#!/bin/sh

SOURCE_FILES_ENCODED=`                          \
    find . \( -name "*.hh"   -o -name "*.cc"    \
           -o -name "*.h"    -o -name "*.c"     \
           -o -name "*.hpp"  -o -name "*.cpp"   \
           -o -name "*.ihpp" -o -name "*.icpp"  \
           -o -name "*.tcc"  -o -name "*.h.in"  \
           -o -name "*.ihpp.in" \
           \) -type f -print0 | xxd -p`

# produces hex dump
#echo -ne "$SOURCE_FILES_ENCODED" | xxd -p -r | od -c

# Prints files to be refactored:
#echo -ne "$SOURCE_FILES_ENCODED" | xxd -p -r | xargs -0 -I '{}' \
#    echo -e "{}"

#echo -ne "$SOURCE_FILES_ENCODED" | xxd -p -r | xargs -0 -I '{}' \
#    grep ::aframe "{}" -n --color

# Stages
#######

# This files one have to rename manually:
#find . -iname "*aframe*" -type f -print

# Rename ::p348 -> ::DPhMC namespace entry
#echo -ne "$SOURCE_FILES_ENCODED" | xxd -p -r | xargs -0 -I '{}' \
#    grep ::aframe "{}" -n --color
#echo -ne "$SOURCE_FILES_ENCODED" | xxd -p -r | xargs -0 -I '{}' \
#    sed -i -e 's/::DPhMC/::DPhMC/g' '{}'
#echo -ne "$SOURCE_FILES_ENCODED" | xxd -p -r | xargs -0 -I '{}' \
#    sed -i -e 's/::p348g4/::DPhMC/g' '{}'

# Rename namespace:
#echo -ne "$SOURCE_FILES_ENCODED" | xxd -p -r | xargs -0 -I '{}' \
#    grep namespace\ aframe "{}" -n --color
#echo -ne "$SOURCE_FILES_ENCODED" | xxd -p -r | xargs -0 -I '{}' \
#    sed -i -e 's/namespace\ p348/namespace DPhMC/g' '{}'
#echo -ne "$SOURCE_FILES_ENCODED" | xxd -p -r | xargs -0 -I '{}' \
#    sed -i -e 's/namespace\ p348g4/namespace DPhMC/g' '{}'

#echo -ne "$SOURCE_FILES_ENCODED" | xxd -p -r | xargs -0 -I '{}' \
#    grep '\<aframe\>' "{}" -n --color
#echo -ne "$SOURCE_FILES_ENCODED" | xxd -p -r | xargs -0 -I '{}' \
#    sed -i -e 's/\<p348\>/DPhMC/g' '{}'
#echo -ne "$SOURCE_FILES_ENCODED" | xxd -p -r | xargs -0 -I '{}' \
#    sed -i -e 's/\<p348g4\>/DPhMC/g' '{}'

# Sentinel macro:
#echo -ne "$SOURCE_FILES_ENCODED" | xxd -p -r | xargs -0 -I '{}' \
#    sed -i -e "s/\(H_\)P348G4\([^[:space:]]\+_H\)/\1DPHMC\2/g" '{}'
#echo -ne "$SOURCE_FILES_ENCODED" | xxd -p -r | xargs -0 -I '{}' \
#    sed -i -e "s/\(H_\)P348\([^[:space:]]\+_H\)/\1DPHMC\2/g" '{}'

# variaous aframe_log?() macros:
#echo -ne "$SOURCE_FILES_ENCODED" | xxd -p -r | xargs -0 -I '{}' \
#    sed -i -e "s/aframe_log\([^[:space:]]\)/sV_log\1/g" '{}'
#echo -ne "$SOURCE_FILES_ENCODED" | xxd -p -r | xargs -0 -I '{}' \
#    sed -i -e "s/DPhMC_log\([^[:space:]]\)/DPhMC_msg\1/g" '{}'

#echo -ne "$SOURCE_FILES_ENCODED" | xxd -p -r | xargs -0 -I '{}' \
#    sed -i -e "s/for_all_p348_lib\([^[:space:]]\)/for_all_PhMClib\1/g" '{}'

#echo -ne "$SOURCE_FILES_ENCODED" | xxd -p -r | xargs -0 -I '{}' \
#    sed -i -e "s/_p348_CST\([^[:space:]]\)/_DPhMC_CST\1/g" '{}'

echo -ne "$SOURCE_FILES_ENCODED" | xxd -p -r | xargs -0 -I '{}' \
    sed -i -e 's/\<emraise\>/DPhMC_exception_throw/g' '{}'

