# ⚠ This is deprecated and is about to be removed in 2021 ⚠

# Modular A' physics for Geant4

This project implements extensions in frame of Geant4 software and devoted to
Monte-Carlo simulations of so-called A' particle (aka $\gamma'$, dark photon,
massive photon).

The project is developed and maintained by NA64 collaboration in CERN.

