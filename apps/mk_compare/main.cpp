/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * This application performs comparison of computed cross sections of A'
 * generators from MK's na64-tools/geant4/simulation package and
 * DarkPhotonMCPhys library.
 */

# include "evGen/aprimeCrossSection.h"
# include "DarkPhotons.hh"

# include <stdio.h>

int
main(int argc, char * const argv[]) {

    // This parameters are generator-irrelevant:
    const uint16_t materialZ = 74;  // Target material nucleus Z
    double  materialA = 183.9,      // Target material nucleus A
            massA_GeV = 0.0167,     // Supposed mass of A'/X-boson particle
            EBeam_GeV = 100,        // Energy of incident particle, GeV
            epsilon = 0.000316,     // Mixing parameter
            factor = 1              // Cross section scaling factor
            ;

    // This parameters relates to DarkPhotonMCPhys generator internals and may
    // be varied in order to fine tune the computation performance and
    // precision:
    double  epsabs = 1e-12,     // QAGS absolute error
            epsrel = 1e-12,     // QAGS relative error
            erelst = 1.1        // QAGS relative error increasing factor
            ;
    size_t  limit = 1e3,        // QAGS maximum number of sub-intervals
            nnodes = 1e3        // GSL integration workspace nodes N
            ;

    //
    // Initialize MK's class
    DarkPhotons * dph = new DarkPhotons(
            /*double MAIn .............. */ massA_GeV,
            /*double EThreshIn ......... */ 1.,  // Threshold energy for electron to emit dark photon
            /*double SigmaNormIn=1. .... */ factor,
            /*double ANuclIn=207. ...... */ materialA,
            /*double ZNuclIn=84. ....... */ materialZ,
            /*double DensityIn=11.35 ... */ 19.25,
            /*double epsilIn=0.0001 .... */ epsilon
        );

    double eTabFactors[] = { .1, .15, .2, .25, .5, .6, .7, .8, .9, 1. };
    for( uint8_t i = 0; i < sizeof(eTabFactors)/sizeof(double); ++i ) {
        void * wsPtr = NULL;
        init_aprime_cross_section_workspace(
                materialA, materialZ, massA_GeV, eTabFactors[i]*EBeam_GeV, epsilon, factor,
                epsabs, epsrel, erelst, limit, nnodes,
                &wsPtr
            );

        double est1 = dph->TotalCrossSectionCalc( eTabFactors[i]*EBeam_GeV ),
               est2 = analytic_integral_estimation( wsPtr );

        printf( "%.3e %.3e\n", est1, est2 );

        free_aprime_cross_section_workspace( wsPtr );
    }

    return EXIT_SUCCESS;
}

